SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `mz_push_users` 
ADD COLUMN `endpoint_arn` VARCHAR(255) NULL DEFAULT NULL COMMENT ' /* comment truncated */ /*AWS EndpointArn*/' AFTER `user_agent`,
ADD COLUMN `subscription_arn` VARCHAR(255) NULL DEFAULT NULL COMMENT ' /* comment truncated */ /*AWS SubscriptionArn*/' AFTER `endpoint_arn`;

ALTER TABLE `mz_push_queue` 
ADD COLUMN `endpoint_arn` VARCHAR(255) NULL DEFAULT NULL COMMENT ' /* comment truncated */ /*AWS EndpointArn*/' AFTER `action`,
ADD COLUMN `subscription_arn` VARCHAR(255) NULL DEFAULT NULL COMMENT ' /* comment truncated */ /*AWS SubscriptionArn*/' AFTER `endpoint_arn`;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
