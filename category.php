<?php
session_start();
require 'util/include.php';

$detect = new Mobile_Detect();
if ($detect->isMobile() || $detect->isTablet()) {
    
} else {
    redirect(LP_PATH . 'pc.html');
}

$sysdate = date('Y-m-d', time());
$systime = date('Y-m-d H:i:s', time());

$sql = sprintf("select * from mz_category where del_flg = 0");
$result_category = mysqli_query($link, $sql);

$rands = rand(1, 10);
?>

<!doctype html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,minimal-ui">
        <title>漫ZOKU</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
        <script src="js/modal.js"></script>
        <script src="js/cmn.js"></script>
        <script src="js/jquery.flexslider-min.js"></script>
        <script src="js/modernizr-transitions.js"></script>
        <script src="js/jquery.masonry.js"></script>
        <script src="js/jquery.min.js" ></script>
        <script src="js/jquery.leanModal.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //$('#aOpen').leanModal({ top: 100, closeButton: ".modal_close" });
                $('a[rel*=leanModal]').leanModal({top: 100, closeButton: ".modal_close"});
            });
        </script>
        <link href="css/base.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/flexslider.css" rel="stylesheet" media="all">
        <link rel="shortcut icon" href="images/favicon.ico">
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
        <style type="text/css"> 
            /* レクタングルボックス */
            #lean_overlay { position: fixed; z-index: 100; top: 0px; left: 0px; height: 100%; width: 100%; background: #000; display: none; }
            .modal_close {
                background: url("images/modal_close.png") repeat scroll 0 0 transparent;
                display: block;
                height: 14px;
                position: relative;
                top:-12px;
                right: 0px;
                bottom: 14px;
                width: 14px;
                z-index: 2;
            }
        </style>
    </head>
    <body>
        <header class="clearfix">
            <h1><a href="<?php echo LP_PATH ?>"><img src="images/logo.png" alt="漫ZOKU"/></a></h1>
            <h2>
                <span style="font-size:10px;line-height:1.5em;color:#EFEFEF;">
                    毎日新着更新！<br>
                    TL・BL・成年漫画<br>
                </span>
                <b style="font-size:10px;line-height:1.5em;">無料で読み放題！</b>
            </h2>
            <p class="button-toggle">HELP</p>
            <div class="menu" style="display:none;">
                <ul>
                    <li><a href="about.html">「漫ZOKU」ってなに？</a></li>
                    <li><a href="policy.html">利用規約</a></li>
                    <li><a href="faq.html">よくある質問</a></li>
                    <li><a href="https://www.mangazokuzoku.com/web_push/">お知らせの通知</a></li>
                    <li><a href="mailto:support@mangazokuzoku.com?subject=お問い合わせ">お問い合わせ</a></li>

                </ul>
            </div>
        </header>
        <nav>
            <ul class="clearfix">
                <li id="nav01"><a href="rank.php"><img  style="min-width: 160px;width:50%;max-height:50px;height:auto;"src='images/rank_pink.png'></a></li>
                <li id="nav02"><a href="category_list.php"><img  style="min-width: 160px;width:50%;max-height:50px;height:auto;"src='images/genre_pink.png'></a></li>
            </ul>
        </nav>
        <section id="category">
            <ul class="clearfix">
                <?php
                while ($arr_list_row = mysqli_fetch_array($result_category)) {
                    echo"
                <li><a href='search.php?action=category&cat_id=" . $arr_list_row[cat_id] . "&cat_name=" . $arr_list_row[cat_name] . "'>" . $arr_list_row[cat_name] . "</a></li>
                ";
                }
                ?>
            </ul>
        </section>

        <!-- レクタングル　広告バーナー⑯-->
        <div style="display: none;" >
            <a id="OpenAdPop" href="#adpop" rel="leanModal"></a>
        </div>

        <div id="adpop" style="background: none repeat scroll 0 0 #FFFFFF; border-radius: 5px 5px 5px 5px;
             box-shadow: 0 0 4px rgba(0, 0, 0, 0.7); display: none; padding-bottom: 2px;
             opacity: 1; position:absolute;width:300px;height:250px;
             top: 50%;left:50%;margin-left:-150px;margin-height:-125px;z-index: 11000;">

            <div style='margin: 0 auto; width: 300px;height:250px;'>
                <?php if ($_SESSION['ad_flg'] == '0') { ?>
                    <!--  ad tags Size: 300x250 ZoneId:1051132-->
                    <script type="text/javascript" src="http://js.speead.jp/t/051/132/a1051132.js"></script>
                <?php } else { ?>

                <?php } ?>
            </div>
            <!--クローズボタン-->
            <a href="#" class="modal_close"></a>

        </div>

        <!-- end -->

        <footer>
            <p><small>&#169; 漫ZOKU</small></p>
        </footer>
        <script>
                        $(function() {
                            $(".menu").css("display", "none");
                            $(".button-toggle").on("click", function() {
                                $(".menu").slideToggle();
                            });
                        });
        </script> 
        <?php if ($rands <= 5) { ?>
            <script>
                $(function() {
                    $('#OpenAdPop').trigger('click');
                });
            </script>
        <?php } ?>
    </body>
    <?php include_once("analyticstracking.php") ?>
</html>