$(function() {

    //データを取得するphpページ
    var action_code = $(':hidden[name="action_code"]').val();
    var cat_id = $(':hidden[name="cat_id"]').val();
    var cat_name = $(':hidden[name="cat_name"]').val();
    var b_auth = $(':hidden[name="b_auth"]').val();
    var searchBox = $(':hidden[name="searchBox"]').val();
    var url = "/search_control.php?action=" + action_code + "&cat_id=" + cat_id + "&cat_name=" + cat_name + "&b_auth=" + b_auth + "&searchBox=" + searchBox;

    //最終ページ
    var last_page = 99999;

    //表示領域のID
    var ViewID = "#Image_ViewArea";

    //初期の取得ページ
    var first_get_page = 1;

    //現在ページ
    var page_num = 1;

    //1頁の高さ
    var AreaHeight;

    //スクロール位置（ピクセル）
    var sp;

    //スクロール位置（ページ）
    var scroll_page = 1;

    //エラーフラグ
    var empty_flg = true;

    /**
     * 初回ロード時に読み込まれるデータ
     */
    $(document).ready(function() {
        if (first_get_page >= 1) {
            ajaxUpdate(page_num, first_get_page);
        }
    });

    /**
     * ページの読み込み処理
     * 
     * @param  page_num  読み込み予定のページNo.
     * @param  pages     残りのページ数（or undefined）
     * @returns {undefined}
     */
    function ajaxUpdate(page_num, pages) {

        if (last_page === undefined || last_page >= page_num) {
            //alert("ページ読み込み（" + page_num + "/" + last_page + "）");
        }
        else {
            return;
        }
        last_page = $(':hidden[name="last_page"]').val();

        var pages;
        if (!pages) {
            pages = 1;
        }
        var pageUrl = url + '&PAGE_NUM=' + page_num;

        if (empty_flg == true) {
            $.ajax({
                url: pageUrl,
                cache: false,
                success: function(data) {
                    if (data) {
                        $('#Image_ViewArea').append(data);

                        //表示エリアの高さをセットする
                        if (!AreaHeight) {
                            AreaHeight = $('.page_block:eq(0)').outerHeight();
                        }

                        //複数ページを取得するとき（ロード時の取得）
                        pages = pages - 1;
                        if (pages != 0) {
                            page_num = page_num + 1;

                            set_page_num(page_num);
                            ajaxUpdate(page_num, pages);
                        }
                    } else {
                        //alert("画像取得エラー");
                        empty_flg = false;
                    }
                }
            });
        }
    }

    /**
     * 読み込みが完了したページNo.を保存
     * 
     * @param  num ページNo.
     */
    function set_page_num(num) {
        page_num = num;
    }

    /**
     * スクロールする度に発生する
     */
    $(window).scroll(function() {
        sp = $(this).scrollTop();
        if (Math.ceil(sp / AreaHeight) > scroll_page) {
            page_num = page_num + 1;
            ajaxUpdate(page_num);
            scroll_page = scroll_page + 1;
        }
    });
});




