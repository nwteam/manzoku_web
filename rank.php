<?php
require 'util/include.php';

$detect = new Mobile_Detect();
if ($detect->isMobile() || $detect->isTablet()) {
    
} else {
    redirect(LP_PATH . 'pc.html');
}

$sysdate = date('Y-m-d', time());
$systime = date('Y-m-d H:i:s', time());

$rankArray = array();
$cnt = 0;

$rankArray[$cnt]['id'] = "new-contents";
$rankArray[$cnt]['title'] = "週間総合ランキング";
$rankArray[$cnt]['db'] = mysqli_query($link, "SELECT DISTINCT book_id  FROM mz_book_read_cnt order by read_cnt desc LIMIT 0,10");

//週間BLランキング
$sql = "SELECT DISTINCT book_id FROM mz_book_read_cnt WHERE book_id IN ( SELECT book_id FROM mz_book where find_in_set('CT001',cat_id)) order by read_cnt desc LIMIT 0,10";
$cnt++;
$rankArray[$cnt]['id'] = "new-contents-bl";
$rankArray[$cnt]['title'] = "週間BLランキング";
$rankArray[$cnt]['db'] = mysqli_query($link, $sql);

//カテゴリプルダウンリスト取得
$result_list_cate = mysqli_query($link, "select * from mz_category WHERE 1 and del_flg=0");

$cateNames = array();
while ($arr_list_row = mysqli_fetch_array($result_list_cate)) {
    $arr_category_row[] = $arr_list_row;
    $cateId = $arr_list_row['cat_id'];
    $cateNames[$cateId] = $arr_list_row['cat_name'];
}

$action = $_GET['action'];
$rands = rand(1, 10);
//Update
if ($action == 'readCnt') {

    $b_id = $_GET['b_id'];
    $b_name = $_GET['b_name'];
    $b_auth = $_GET['b_auth'];
    $year = date('Y', time());
    $month = date('m', time());

    $sql = sprintf("select count(*) cnt from mz_book_read_cnt where book_id = '%s' and year='%s' and month='%s'", $b_id, $year, $month);
    $result_bc = mysqli_query($link, $sql);
    $rs_bc = mysqli_fetch_object($result_bc);
    $cnt = $rs_bc->cnt;
    if ($cnt == '0') {
        $sql = sprintf("insert into mz_book_read_cnt (book_id,year,month,read_cnt) values 
						('%s','%s','%s',1)"
            , $b_id, $year, $month);
        $result = mysqli_query($link, $sql);
    } else {
        $sql = sprintf("select read_cnt from mz_book_read_cnt where book_id = '%s' and year='%s' and month='%s'", $b_id, $year, $month);
        $result = mysqli_query($link, $sql);
        $rs = mysqli_fetch_object($result);
        $read_cnt = ($rs->read_cnt) + 1;
        $sql = sprintf("UPDATE mz_book_read_cnt SET read_cnt=%d WHERE book_id = '%s' and year='%s' and month='%s'", $read_cnt, $b_id, $year, $month);
        $result = mysqli_query($link, $sql);
    }
    $url = "chapter.php?b_id=" . $b_id . "&b_name=" . urlencode($b_name) . "&b_auth=" . urlencode($b_auth);
    redirect($url);
}
?>
<!doctype html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="keywords" content="マンガ,漫画,まんが,コミック,漫画アプリ,電子書籍,無料,全巻,Webコミック,Webマンガ,book,comic,BL,TL,ボーイズラブ,ティーンズラブ,人気マンガ,人気漫画,まんぞく,manzoku,漫ZOKU,漫zoku,マンゾク">
        <meta name="description" content="「漫ZOKU」とは全巻無料でマンガが読める電子書籍サービスです。話題のwebマンガから、人気漫画雑誌で連載していた不朽の名作まで、様々な種類のマンガを無料でお読みいただけます。">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,minimal-ui">
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <meta name="apple-touch-fullscreen" content="no"/>
        <title>漫ZOKU</title>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script src="js/modal.js"></script>
        <script src="js/cmn.js"></script>
        <!--<script src="js/jquery.flexslider-min.js"></script>-->

        <script src="js/modernizr-transitions.js"></script>
        <script src="js/jquery.masonry.js"></script>
        <script src="js/imagesloaded.pkgd.min.js"></script>
        <script src="js/jquery.leanModal.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/jquery.meerkat.1.3.min.js"></script>
        <script src="js/flipsnap.js"></script>
        <script src="js/flipsnap_rank_script.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //$('#aOpen').leanModal({ top: 100, closeButton: ".modal_close" });
                $('a[rel*=leanModal]').leanModal({top: 100, closeButton: ".modal_close"});
            });
        </script>
        <link href="css/base.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/rank.css" rel="stylesheet" type="text/css" media="all">
        <link rel="stylesheet" type="text/css" href="css/meerkat.css">
        <link href="css/flexslider.css" rel="stylesheet" media="all">
        <link rel="shortcut icon" href="images/favicon.ico">
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
        <style type="text/css"> 
            /* レクタングルボックス */
            #lean_overlay { position: fixed; z-index: 100; top: 0px; left: 0px; height: 100%; width: 100%; background: #000; display: none; }
            .modal_close {
                background: url("images/modal_close.png") repeat scroll 0 0 transparent;
                display: block;
                height: 14px;
                position: relative;
                top:-12px;
                right: 0px;
                bottom: 14px;
                width: 14px;
                z-index: 2;
            }
        </style>
        <script type="text/javascript">
            window.onload = function() {
                setTimeout(scrollTo, 100, 0, 1);
            };
        </script>
        <script type="text/javascript">
            $(function() {
                $('.meerkat').meerkat({
                    height: '50px',
                    width: '100%',
                    position: 'top',
                    close: '.close-meerkat',
                    dontShowAgain: '.dont-show',
                    animationIn: 'slide',
                    animationSpeed: 500,
                    removeCookie: '.reset'
                }).addClass('pos-bot');
            });
        </script>
    </head>
    <body>
        <div style="margin: 0 auto; height:50px;background:#666;">&nbsp;</div>
        <header class="clearfix">
            <h1><a href="<?php echo LP_PATH ?>"><img src="images/logo.png" alt="漫ZOKU"/></a></h1>
            <h2>
                <span style="font-size:10px;line-height:1.5em;color:#EFEFEF;">
                    毎日新着更新！<br>
                    TL・BL・成年漫画<br>
                </span>
                <b style="font-size:10px;line-height:1.5em;">無料で読み放題！</b>
            </h2>
            <p class="button-toggle">HELP</p>
            <div class="menu" style="display:none;">
                <ul>
                    <li><a href="about.html">「漫ZOKU」ってなに？</a></li>
                    <li><a href="policy.html">利用規約</a></li>
                    <li><a href="faq.html">よくある質問</a></li>
                    <li><a href="https://www.mangazokuzoku.com/web_push/">お知らせの通知</a></li>
                    <li><a href="mailto:support@mangazokuzoku.com?subject=お問い合わせ">お問い合わせ</a></li>
                </ul>
            </div>
        </header>
        <!-- 広告バーナー　⑰ -->
        <div class="meerkat">
            <div class="adsense" >
                <a href="#" class="close-meerkat">close</a>

                <?php if ($_SESSION['ad_flg'] == '0') { ?>
                <?php } else { ?>

                <?php } ?>

            </div></div>
        <section id="search">
            <form method="post" class="clearfix" action="search.php?action=list" >
                <input type="search" data-mini="true" data-clear-btn-text="入力クリア" value="<?php echo $searchBox; ?>" placeholder=""name="searchBox" id="searchBox">
                <input type="submit" id="send" value="検索">
            </form>
        </section>
        <nav>
            <ul class="clearfix">
                <li id="nav01"><a href="rank.php"><img  style="min-width: 160px;width:50%;max-height:50px;height:auto;"src='images/rank_pink.png'></a></li>
                <li id="nav02"><a href="category_list.php"><img  style="min-width: 160px;width:50%;max-height:50px;height:auto;"src='images/genre_pink.png'></a></li>
            </ul>
        </nav>



        <?php foreach ($rankArray as $rankInfo) { ?>
            <section id="<?php echo $rankInfo['id'] ?>">
                <h2><?php echo $rankInfo['title'] ?></h2>

                <!-- 画像 -->
                <div class="viewport">
                    <div class="flipsnap">
                        <?php
                        $k = 0;
                        while ($row = mysqli_fetch_array($rankInfo['db'])) {
                            $r_book_info = mysqli_query($link, "SELECT * FROM mz_book WHERE book_id = '" . $row['book_id'] . "'");
                            $chapter = mysqli_fetch_array($r_book_info);
                            $k++;
                            ?>
                            <?php $newPageUrl = "index.php?action=readCnt&b_id=" . $chapter['book_id'] . "&b_name=" . $chapter['book_name'] . "&b_auth=" . $chapter['book_auth']; ?>
                            <?php $newImgUrl = COMIC_PATH . $chapter['cover_img_path']; ?>
                            <?php $newBookName = $chapter['book_name'] ?>
                            <?php $newBookAuth = $chapter['book_auth'] ?>
                            <div class="item_group">
                                <div class="item">
                                    <a href='<?php echo $newPageUrl ?>'>
                                        <img src='images/rank<?php echo $k ?>.png' class='lankLabel' alt='<?php echo $k ?>位' />
                                        <img src='<?php echo $newImgUrl ?>' alt='<?php echo $newBookName ?>' />
                                        <dl>
                                            <dt class='title'><?php echo $newBookName ?></dt>
                                            <dd class='name'><?php echo $newBookAuth ?></dd>
                                        </dl>
                                    </a>
                                </div>
                                <ul class='category'>
                                    <?php
                                    /* ジャンル */
                                    $cateIds = explode(",", $chapter['cat_id']);
                                    if (!empty($cateIds)) {
                                        foreach ($cateIds as $cateId) {
                                            ?>  
                                            <li>
                                                <a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo $cateNames[$cateId] ?>'><?php echo $cateNames[$cateId] ?></a>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!--// 画像 -->
            </section>
        <?php } ?>


        <!--/.bnr-->
        <div style="margin: 0 auto; width: 320px;"><!--広告バナー①-->

            <?php if ($_SESSION['ad_flg'] == '0') { ?>

            <?php } else { ?>

            <?php } ?>
        </div><!--M-->

        <!-- レクタングル　広告バーナー⑪-->
        <div style="display: none;" >
            <a id="OpenAdPop" href="#adpop" rel="leanModal"></a>
        </div>

        <div id="adpop" style="background: none repeat scroll 0 0 #FFFFFF; border-radius: 5px 5px 5px 5px;
             box-shadow: 0 0 4px rgba(0, 0, 0, 0.7); display: none; padding-bottom: 2px;
             opacity: 1; position:absolute;width:300px;height:250px;
             top: 50%;left:50%;margin-left:-150px;margin-height:-125px;z-index: 11000;">

            <div style='margin: 0 auto; width: 300px;height:250px;'>
                <?php if ($_SESSION['ad_flg'] == '0') { ?>
                    <!--  ad tags Size: 300x250 ZoneId:1051132-->
                    <script type="text/javascript" src="http://js.speead.jp/t/051/132/a1051132.js"></script>
                <?php } else { ?>   
                <?php } ?>
            </div>
            <!--クローズボタン-->
            <a href="#" class="modal_close"></a>

        </div>

        <footer>
            <p><small>&#169; 漫ZOKU</small></p>
        </footer>
        <script>
                        $(function() {
                            $(".menu").css("display", "none");
                            $(".button-toggle").on("click", function() {
                                $(".menu").slideToggle();
                            });
                        });
        </script>
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-59561621-1', 'auto');
            ga('send', 'pageview');

        </script>

        <!-- レクタングルポップアップ確率 -->
        <?php if ($rands <= 10) { ?>
            <script>
                $(function() {
                    $('#OpenAdPop').trigger('click');
                });
            </script>
        <?php } ?>
    </body>
</html>
