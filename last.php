<?php
session_start();
require 'util/include.php';

$detect = new Mobile_Detect();
if ($detect->isMobile() || $detect->isTablet()) {
    
} else {
    redirect(LP_PATH . 'pc.html');
}

$sysdate = date('Y-m-d', time());
$systime = date('Y-m-d H:i:s', time());

$b_id = $_GET['b_id']; //ブックID
$c_id = $_GET['c_id']; //チャプターID
$b_name = $_GET['b_name']; //ブック名

$sql = sprintf("select * from mz_last_ad where del_flg = 0 order by id");
$result_ad = mysqli_query($link, $sql);

//トータル話数取得
//$sql = sprintf("SELECT chapter_id FROM mz_chapter WHERE insert_time<'%s'", time());
$sql = sprintf("SELECT MAX(chapter_id) as total_chap FROM mz_chapter WHERE book_id='%s'", $b_id);
$result_total_chap = mysqli_query($link, $sql);
$total_chap_row = mysqli_fetch_array($result_total_chap);
$total_chap = $total_chap_row['total_chap'];

//次の話の配信日付を取得
$sql = sprintf("SELECT insert_time FROM mz_chapter WHERE book_id='%s' AND chapter_id=%d", $b_id, $c_id + 1);
$result_insert_time = mysqli_query($link, $sql);
$total_time_row = mysqli_fetch_array($result_insert_time);
$nextInsertTime = $total_time_row['insert_time'];

//作者情報取得
$sql = sprintf("SELECT * FROM mz_book WHERE book_id='%s'", $b_id);
$result_auth = mysqli_query($link, $sql);
$book_auth_row = mysqli_fetch_array($result_auth);
$book_auth = $book_auth_row['book_auth'];

$rands = rand(1, 10);

$action = $_GET['action'];
//Update
if ($action == 'readCnt') {

    $b_id = $_GET['b_id'];
    $c_id = $_GET['c_id'];
    $b_name = $_GET['b_name'];
    $i = $_GET['i'];
    $year = date('Y', time());
    $month = date('m', time());

    $sql = sprintf("select count(*) cnt from mz_chapter_read_cnt where book_id = '%s' and chapter_id = %d and year='%s' and month='%s'", $b_id, $c_id, $year, $month);
    $result_cc = mysqli_query($link, $sql);
    $rs_cc = mysqli_fetch_object($result_cc);
    $cnt = $rs_cc->cnt;

    if ($cnt == '0') {
        $sql = sprintf("insert into mz_chapter_read_cnt (book_id,chapter_id,year,month,read_cnt) values 
						('%s',%d,'%s','%s',1)"
            , $b_id, $c_id, $year, $month);
        $result = mysqli_query($link, $sql);
    } else {
        $sql = sprintf("select read_cnt from mz_chapter_read_cnt where book_id = '%s' and chapter_id = %d and year='%s' and month='%s'", $b_id, $c_id, $year, $month);
        $result = mysqli_query($link, $sql);
        $rs = mysqli_fetch_object($result);
        $read_cnt = ($rs->read_cnt) + 1;

        $sql = sprintf("UPDATE mz_chapter_read_cnt SET read_cnt=%d WHERE book_id = '%s' and chapter_id = %d and year='%s' and month='%s'", $read_cnt, $b_id, $c_id, $year, $month);
        $result = mysqli_query($link, $sql);
    }

    $url = "chapter_read.php?b_id=" . $b_id . "&c_id=" . $c_id . "&b_name=" . urlencode($b_name) . "&b_auth=" . urlencode($book_auth);
    redirect($url);
}
?>
<!doctype html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="keywords" content="マンガ,漫画,まんが,コミック,漫画アプリ,電子書籍,無料,全巻,Webコミック,Webマンガ,book,comic,BL,TL,ボーイズラブ,ティーンズラブ,人気マンガ,人気漫画,まんぞく,manzoku,漫ZOKU,漫zoku,マンゾク">
        <meta name="description" content="「漫ZOKU」とは全巻無料でマンガが読める電子書籍サービスです。話題のwebマンガから、人気漫画雑誌で連載していた不朽の名作まで、様々な種類のマンガを無料でお読みいただけます。">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,minimal-ui">
        <title>漫ZOKU</title>
        <script src="js/jquery.min.js" ></script>
        <script src="js/modal.js"></script>
        <script src="js/cmn.js"></script>
        <script src="js/jquery.flexslider-min.js"></script>
        <script src="js/modernizr-transitions.js"></script>
        <script src="js/jquery.masonry.js"></script>

        <script type="text/javascript" src="js/jquery.meerkat.1.3.min.js"></script>
        <script src="js/jquery.leanModal.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //$('#aOpen').leanModal({ top: 100, closeButton: ".modal_close" });
                $('a[rel*=leanModal]').leanModal({top: 100, closeButton: ".modal_close"});
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('.meerkat').meerkat({
                    height: '50px',
                    width: '100%',
                    position: 'top',
                    close: '.close-meerkat',
                    dontShowAgain: '.dont-show',
                    animationIn: 'slide',
                    animationSpeed: 500,
                    removeCookie: '.reset'
                }).addClass('pos-bot');
            });
        </script>
        <link href="css/base.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/flexslider.css" rel="stylesheet" media="all">
        <link rel="stylesheet" type="text/css" href="css/meerkat.css">
        <link rel="shortcut icon" href="images/favicon.ico">
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
        <style type="text/css"> 
            /* レクタングルボックス */
            #lean_overlay { position: fixed; z-index: 100; top: 0px; left: 0px; height: 100%; width: 100%; background: #000; display: none; }
            .modal_close {
                background: url("images/modal_close.png") repeat scroll 0 0 transparent;
                display: block;
                position: relative;
                top:-12px;
                right: 0px;
                bottom: 14px;
                width: 14px;
                height: 14px;
                z-index: 2;
            }
        </style>
    </head>
    <body>
        <header class="clearfix">
            <h1><a href="<?php echo LP_PATH ?>"><img src="images/logo.png" alt="漫ZOKU"/></a></h1>
            <p class="button-toggle">HELP</p>
            <div class="menu" style="display:none;">
                <ul>
                    <li><a href="about.html">「漫ZOKU」ってなに？</a></li>
                    <li><a href="policy.html">利用規約</a></li>
                    <li><a href="faq.html">よくある質問</a></li>
                    <li><a href="https://www.mangazokuzoku.com/web_push/">お知らせの通知</a></li>
                    <li><a href="mailto:support@mangazokuzoku.com?subject=お問い合わせ">お問い合わせ</a></li>
                </ul>
            </div>
        </header>
        <section id="lastPage">

            <h2>最終話まで随時更新中！！</h2>

            <div class="toBackTitle">
                <?php if ($total_chap > $c_id) { ?>                      
                    <?php if ($nextInsertTime <= time()) { ?>
                        <a href="#" onclick="readCnt('<?php echo $b_id; ?>', '<?php echo (int) $c_id + 1; ?>', '<?php echo $b_name; ?>', '1', '<?php echo $book_auth ?>')" >次の話へ</a>
                    <?php } else { ?>
                        <b style="padding-left:4px;padding-right:4px;"><?php echo date("m/d更新", $nextInsertTime) ?></b>
                    <?php } ?>                    
                <?php } else { ?>
                    <b>次の話へ</b>
                <?php } ?>

                <a href="chapter.php?b_id=<?php echo $b_id; ?>&b_name=<?php echo urlencode($b_name); ?>&b_auth=<?php echo urlencode($book_auth); ?>">他の話を読む</a>

                <?php if ($c_id > 1) { ?>
                    <a href="#" onclick="readCnt('<?php echo $b_id; ?>', '<?php echo (int) $c_id - 1; ?>', '<?php echo $b_name; ?>', '1', '<?php echo $book_auth ?>')" >前の話へ</a>
                <?php } else { ?>
                    <b>前の話へ</b>
                <?php } ?>
                <div style="clear:both;"></div>
            </div>

            <h3>その他 ”おすすめ” はコチラ！</h3>
            <div id="titleList">
                <!--<iframe src="http://ad.duga.jp/dynamic/24004/07/" marginwidth="0" marginheight="0" width="240" height="295" border="0" frameborder="0" style="border:none;" scrolling="no"><a href="http://click.duga.jp/24004-01" target="_blank">アダルト動画 DUGA -デュガ-</a></iframe>-->
                <iframe src="http://ad.duga.jp/dynamic/24004/08/?mode=1" marginwidth="0" marginheight="0" width="240" height="295" border="0" frameborder="0" style="border:none;" scrolling="no"><a href="http://click.duga.jp/24004-01" target="_blank">アダルト動画 DUGA -デュガ-</a></iframe>
                <iframe src="http://ad.duga.jp/dynamic/24004/09/?mode=1" marginwidth="0" marginheight="0" width="240" height="295" border="0" frameborder="0" style="border:none;" scrolling="no"><a href="http://click.duga.jp/24004-01" target="_blank">アダルト動画 DUGA -デュガ-</a></iframe>
                <iframe src="http://ad.duga.jp/dynamic/24004/10/?mode=1" marginwidth="0" marginheight="0" width="240" height="295" border="0" frameborder="0" style="border:none;" scrolling="no"><a href="http://click.duga.jp/24004-01" target="_blank">アダルト動画 DUGA -デュガ-</a></iframe>
                <iframe src="http://ad.duga.jp/dynamic/24004/11/?mode=1" marginwidth="0" marginheight="0" width="240" height="295" border="0" frameborder="0" style="border:none;" scrolling="no"><a href="http://click.duga.jp/24004-01" target="_blank">アダルト動画 DUGA -デュガ-</a></iframe>
                <iframe src="http://ad.duga.jp/dynamic/24004/12/?mode=1" marginwidth="0" marginheight="0" width="240" height="295" border="0" frameborder="0" style="border:none;" scrolling="no"><a href="http://click.duga.jp/24004-01" target="_blank">アダルト動画 DUGA -デュガ-</a></iframe>
                <iframe src="http://ad.duga.jp/dynamic/24004/13/?mode=1" marginwidth="0" marginheight="0" width="240" height="295" border="0" frameborder="0" style="border:none;" scrolling="no"><a href="http://click.duga.jp/24004-01" target="_blank">アダルト動画 DUGA -デュガ-</a></iframe>
                <iframe src="http://ad.duga.jp/dynamic/24004/14/?mode=1" marginwidth="0" marginheight="0" width="240" height="295" border="0" frameborder="0" style="border:none;" scrolling="no"><a href="http://click.duga.jp/24004-01" target="_blank">アダルト動画 DUGA -デュガ-</a></iframe>
                <iframe src="http://ad.duga.jp/dynamic/24004/15/?mode=1" marginwidth="0" marginheight="0" width="240" height="295" border="0" frameborder="0" style="border:none;" scrolling="no"><a href="http://click.duga.jp/24004-01" target="_blank">アダルト動画 DUGA -デュガ-</a></iframe>
                <iframe src="http://ad.duga.jp/dynamic/24004/16/?mode=1" marginwidth="0" marginheight="0" width="240" height="295" border="0" frameborder="0" style="border:none;" scrolling="no"><a href="http://click.duga.jp/24004-01" target="_blank">アダルト動画 DUGA -デュガ-</a></iframe>
                <iframe src="http://ad.duga.jp/dynamic/24004/17/?mode=1" marginwidth="0" marginheight="0" width="240" height="295" border="0" frameborder="0" style="border:none;" scrolling="no"><a href="http://click.duga.jp/24004-01" target="_blank">アダルト動画 DUGA -デュガ-</a></iframe>
                <iframe src="http://ad.duga.jp/dynamic/24004/18/?mode=1" marginwidth="0" marginheight="0" width="240" height="295" border="0" frameborder="0" style="border:none;" scrolling="no"><a href="http://click.duga.jp/24004-01" target="_blank">アダルト動画 DUGA -デュガ-</a></iframe>
            </div>
        </section>

        <div style="margin: 0 auto; width: 320px;">
            <!-- 広告 -->
            <?php if ($_SESSION['ad_flg'] == '0') { ?>

            <?php } else { ?>

            <?php } ?>
            <!--// 広告 -->
        </div>

        <!-- 広告 -->
        <div style="display: none;" >
            <a id="OpenAdPop" href="#adpop" rel="leanModal"></a>
        </div>
        <div id="adpop" style="background: none repeat scroll 0 0 #FFFFFF; border-radius: 5px 5px 5px 5px;
             box-shadow: 0 0 4px rgba(0, 0, 0, 0.7); display: none; padding-bottom: 2px;
             opacity: 1; position:absolute;width:300px;height:250px;
             top: 50%;left:50%;margin-left:-150px;margin-height:-125px;z-index: 11000;">

            <div style='margin: 0 auto; width: 300px;height:250px;'>
                <?php if ($_SESSION['ad_flg'] == '0') { ?>
                    <!-- ad tags Size: 300x250 ZoneId:1048296-->
                    <script type="text/javascript" src="http://js.speead.jp/t/048/296/a1048296.js"></script>
                <?php } else { ?>

                <?php } ?>
            </div>
            <!--クローズボタン-->
            <a href="#" class="modal_close"></a>
        </div>
        <!--// 広告 -->

        <footer>
            <p><small>&#169; 漫ZOKU</small></p>
        </footer>
        <script>
                    $(function() {
                        $(".menu").css("display", "none");
                        $(".button-toggle").on("click", function() {
                            $(".menu").slideToggle();
                        });
                    });
        </script>
        <script>
            function readCnt(b_id, c_id, b_name, i, b_auth) {
                var pageurl = "chapter.php?action=readCnt&b_id=" + b_id + "&c_id=" + c_id + "&b_name=" + b_name + "&i=" + i + "&b_auth=" + b_auth;
                window.location.href = pageurl;
            }
        </script>

        <!-- レクタングルポップアップ確率 -->
        <?php if ($rands <= 10) { ?>
            <script>
                $(function() {
                    $('#OpenAdPop').trigger('click');
                });
            </script>
        <?php } ?>
    </body>
    <?php include_once("analyticstracking.php") ?>
</html>