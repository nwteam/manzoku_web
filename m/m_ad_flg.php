<?php
	//include
	require '../util/include.php';
    session_start();
	$sub_title='広告管理';
	$systime=date('Y-m-d H:i:s',time());

    $role=$_SESSION['role'];
    $login_user=$_SESSION['login_user'];

	$action = $_GET['action'];

	//Update
	if ($action=='update'){
		$ad_flg = $_POST['ad_flg'];

		$db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysqli_error($db));
		}
		$dns = mysqli_select_db($db,DB_NAME);
		if(!$dns){
			die("connot use db:" . mysqli_error($db));
		}
		mysqli_set_charset($db,'utf8');

		$sql = sprintf("UPDATE mz_setting SET ad_flg=%d",$ad_flg);

		$result = mysqli_query($db,$sql);

		mysqli_close($db);
	}

	//Search
	if ($action=='search'||$action=='update'){

		$db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		mysqli_set_charset($db,'utf8');

		$page_size=1000;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;

		//All
		$sqlall = "select * from mz_setting WHERE 1";

		$result = mysqli_query($db,$sqlall) or die(mysqli_error($db));

		if(!$result){
			$rowCnt = -1;
			mysqli_close($db);
		}
		$rowCntall=mysqli_num_rows($result);

		//Select current all
		$sql = sprintf("%s order by ad_flg limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

		$result = mysqli_query($db,$sql);

		if(!$result){
			$rowCnt = -1;
			mysqli_close($db);
		}

		$rowCnt=mysqli_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			mysqli_close($db);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
	}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/common.js"></script>
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form action='?action=search' method='post' name='form1'>
<?php
if ($rowCnt>0){
	echo "
		<table width='280px' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
              <th width='200px'>広告類型</th>
              <th width='60px'>操作</th>
			</tr>
		</table>
	";
	$i=1;
	while($rs=mysqli_fetch_object($result))
	{
	  $ad_flg=$rs->ad_flg;
	  if ($ad_flg=='0'){
		  echo "
				<table width='280px' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
					<tr align='left' bgcolor='#EEF2F4'>
						<td width='200px'align='center'>
							<select name='ad_flg'  id='ad_flg' style='float:left;width:200px;font-size:16px;'>
								 <option value='0' 'selected' >0：i-mobile</option>
								 <option value='1'>1：nend</option>
							</select>
						</td>
						<td width='60px'align='center'><input type='button' class='buttonS bGreen' value='更新' onclick=\"updateChange()\"/></td>
					</tr>
				 </table>
				";
	  }
	  elseif ($ad_flg=='1'){
		echo "
			<table width='280px' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
				<tr align='left' bgcolor='#EEF2F4'>
					<td width='200px'align='center'>
						<select name='ad_flg'  id='ad_flg' style='float:left;width:200px;font-size:16px;'>
							 <option value='0'>0：i-mobile</option>
							 <option value='1' selected >1：nend</option>
						</select>
					</td>
					<td width='60px'align='center'><input type='button' class='buttonS bGreen' value='更新' onclick=\"updateChange()\"/></td>
                </tr>
             </table>
		    ";
	  }
	  else{
		echo "
			<table width='280px' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
				<tr align='left' bgcolor='#EEF2F4'>
					<td width='200px'align='center'>
						<select name='ad_flg'  id='ad_flg' style='float:left;width:200px;'>
							 <option value='0' selected >0：i-mobile</option>
							 <option value='1'>1：nend</option>
						</select>
					</td>
					<td width='60px'align='center'><input type='button' class='buttonS bGreen' value='更新' onclick=\"updateChange()\"/></td>
                </tr>
             </table>
		    ";
	  }
        
		$i++;
	}

	mysqli_close($db);
}
?>
</form>
<script language="javascript" type="text/javascript">
	function updateChange() {
		document.form1.action="?action=update";
		document.form1.submit();
	}
</script>
</div>
</body>
</html>