<?php
	session_start();
	require '../util/include.php';

	$systime=date('Y-m-d H:i:s',time());
	$ip=get_real_ip();
	
	//error_log("$systime $ip INFO: Login Start \r\n",3,'../log/gen.log');
	if(isSet($_POST['username']) && isSet($_POST['password'])){
		$u_id=$_POST['username'];
		$pwd=$_POST['password'];

		$ret=chkMem($u_id, $pwd);
		
		if($ret == '-1'){
			//sys err
			$err_cd_list[]="99";
			$_SESSION['err_cd_list']=$err_cd_list;
			redirect(URL_PATH . "err.php");
		}
		elseif ($ret == '0'){
			//user err
			echo '0';
		}elseif($ret == '2'){
			//pwd err
			echo '2';
		}
		else{
            $ret=getRole($u_id);
            if($ret == -1){
                //sys err
                $err_cd_list[]="99";
                $_SESSION['err_cd_list']=$err_cd_list;
                redirect(URL_PATH . "err.php");
            }
			$_SESSION['login_user']=$u_id;
            $_SESSION['role']=$ret;
			echo '1';
		}

	}
?>
