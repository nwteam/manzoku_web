<?php
session_start();
require '../util/include.php';
$sub_title = '漫画管理　- 漫画登録 -';
$action = $_GET['action'];
$sysdate = date('Y-m-d', time());
$systime = date('Y-m-d H:i:s', time());
$ip = get_real_ip();

//プルダウンリスト取得
$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
if (!$db) {
    die("connot connect:" . mysqli_error());
}
$dns = mysqli_select_db($db, DB_NAME);
if (!$dns) {
    die("connot use db:" . mysqli_error());
}
mysqli_set_charset($db, 'utf8');
$sqlall = "select * from mz_category WHERE 1 and del_flg=0";
$result_list_cat = mysqli_query($db, $sqlall);
mysqli_close($db);

//insert
if ($action == 'insert') {
    //漫画名
    $i_book_name = $_POST['i_book_name'];
    //漫画の概要
    $i_book_overview = $_POST['i_book_overview'];
    //作者名
    $i_auth = $_POST['i_auth'];
    //フォルダー名
    $i_folder_name = $_POST['i_folder_name'];
    //並び順
    $i_sort_order = $_POST['i_sort_order'];
    //話数
    if ($i_folder_name != '') {
        $folder_path = 'comic/' . $i_folder_name;
        $filesnames = myScandir($folder_path, true);
        sleep(1);
        $chap_cnt = sizeof($filesnames);
    } else {
        $err_cd_list[] = "99";
        $_SESSION['err_cd_list'] = $err_cd_list;
        $url = URL_PATH . "err.php";
        redirect($url);
    }

    //Cover
    $cover_img_path = "comic/" . $i_folder_name . "/title/cover.jpg";
    //カテゴリ
    $i_category_id = $_POST['h_category_id'];

    $db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if (!$db) {
        die("connot connect:" . mysqli_error());
    }
    $dns = mysqli_select_db($db, DB_NAME);
    if (!$dns) {
        die("connot use db:" . mysqli_error());
    }
    mysqli_set_charset($db, 'utf8');
    $logstr = "$systime $ip INFO：▼パス存在チェック開始 \r\n";
    //error_log($logstr, 3, '../log/gen.log');
    //パス存在チェック
    $ret = chkPath($path);
    if ($ret != '0') {
        mysqli_close($db);
        $logstr = "$systime ERR：パス存在チェックエラー！ \r\n";
        $logstr .= "$systime $ip INFO：▲パス存在チェック異常終了 \r\n";
        error_log($logstr, 3, '../log/gen.log');

        $err_cd_list[] = "02";
        $_SESSION['err_cd_list'] = $err_cd_list;
        $url = URL_PATH . "err.php";
        redirect($url);
    }

    $logstr = "$systime $ip INFO：▲パス存在チェック正常終了！！ \r\n";
    //error_log($logstr, 3, '../log/gen.log');

    $logstr = "$systime $ip INFO：▼漫画情報登録開始 \r\n";
    //error_log($logstr, 3, '../log/gen.log');

    $sql = "UPDATE mz_book_id SET id = LAST_INSERT_ID(id+1)";
    $result_id = mysqli_query($db, $sql);

    $sql = "select last_insert_id() as id";
    $result_b_id = mysqli_query($db, $sql);
    $row_b_id = mysqli_fetch_assoc($result_b_id);
    $book_id = "B" . str_pad($row_b_id['id'], 10, "0", STR_PAD_LEFT);

    $sql = sprintf("insert into mz_book (sort_num, book_id, book_name, book_overview, book_auth, total_chap, folder_name, cover_img_path, cat_id, insert_time) 
    										values (%d,'%s','%s','%s','%s',%d,'%s','%s','%s',%d)", $i_sort_order, $book_id, $i_book_name, $i_book_overview, $i_auth, $chap_cnt, $i_folder_name, $cover_img_path, $i_category_id, strtotime($systime)
    );

    $logstr = "$systime $ip INFO：漫画情報登録 INSERT SQL文： " . $sql . "\r\n";
    //error_log($logstr, 3, '../log/gen.log');

    $result = mysqli_query($db, $sql);
    if (!$result) {
        mysqli_close($db);
        $logstr = "$systime ERR：漫画情報DB登録異常！ \r\n";
        $logstr .= "$systime $ip INFO：▲漫画情報登録異常終了 \r\n";
        error_log($logstr, 3, '../log/gen.log');
        $err_cd_list[] = "01";
        $_SESSION['err_cd_list'] = $err_cd_list;
        $url = URL_PATH . "err.php";
        redirect($url);
    }
    $logstr = "$systime $ip INFO：▲漫画情報登録正常終了！！ \r\n";
    //error_log($logstr, 3, '../log/gen.log');

    mysqli_close($db);
    $url = URL_PATH . "m_comic.php?action=search";
    redirect($url);
}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
    <head>
        <title><?php echo $sub_title; ?></title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="cache-control" content="no-cache,must-revalidate" />
        <meta http-equiv="expires" content="Wed, 23 Aug 2006 12:40:27 UTC" />
        <meta http-equiv="content-style-type" content="text/css" />
        <meta http-equiv="content-script-type" content="text/javascript" />

        <link href="../css/common.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="../js/multiselectSrc/jquery.multiselect.css" />
        <link rel="stylesheet" type="text/css" href="../js/assets/style.css" />
        <link rel="stylesheet" type="text/css" href="../js/assets/prettify.css" />
        <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>

        <script type="text/javascript" src="../js/ui/jquery.ui.core.js"></script>
        <script type="text/javascript" src="../js/ui/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="../js/assets/prettify.js"></script>
        <script type="text/javascript" src="../js/multiselectSrc/jquery.multiselect.js"></script>
        <script type="text/javascript">
            $(function() {
                $("#i_category_id").multiselect({
                    noneSelectedText: "選択してください",
                    checkAllText: "全て選択",
                    uncheckAllText: '全て選択解除',
                    selectedList: 3,
                    selectedText: '# 個選択',
                    height: 200,
                    minWidth: 500
                });
            });
        </script>
    </head>
    <body>
        <div class='main'>
            <div class='subtitle'><?php echo $sub_title; ?></div>
            <form enctype='multipart/form-data' method='post' name='upform' action="">
                <div class='input-area'>
                    <label class='w150'>漫画名<span style="color:red">(必須)</span></label>
                    <input type='text' class='w500' name='i_book_name' id='i_book_name' value='<?php echo $i_book_name; ?>'/>
                    <div style='clear:both;'></div>
                    
                    <label class='w150'>漫画の概要</label>
                    <textarea id='i_book_overview' name="i_book_overview"><?php echo $i_book_overview; ?></textarea>
                    <div style='clear:both;'></div>
                    
                    <label class='w150'>作者名</label>
                    <input type='text' class='w500' name='i_auth' id='i_auth' value='<?php echo $i_auth; ?>'/>
                    <div style='clear:both;'></div>
                    
                    <label class='w150'>フォルダー名<span style="color:red">(必須)</span></label>
                    <input type='text' class='w200' name='i_folder_name' id='i_folder_name' value='<?php echo $i_folder_name; ?>'/>
                    <div style='clear:both;'></div>
                    
                    <label class='w150'>カテゴリ</label>
                    <select name='i_category_id' id='i_category_id' multiple="multiple" size="5">
                        <?php
                        $i = 0;
                        while ($arr_list_row = mysqli_fetch_array($result_list_cat)) {
                            if ($arr_list_row[cat_id] == $arr_category_id[$i]) {
                                echo"<option value=" . $arr_list_row[cat_id] . " selected >" . $arr_list_row[cat_name] . "</option>";
                                $i++;
                            } else {
                                echo"<option value=" . $arr_list_row[cat_id] . " >" . $arr_list_row[cat_name] . "</option>";
                            }
                        }
                        ?>
                    </select>
                    <div style='clear:both;'></div>
                    <label class='w150'>並び順番</label>
                    <input type='text' class='w100' name='i_sort_order' id='i_sort_order'  value='<?php echo $i_sort_order; ?>'/>
                    <div style='clear:both;'></div>
                    <input type='button' class='buttonS bGreen ml190 w200 mt40' value='登録' onclick='moveConfirm();'/>
                    <input type="hidden" name='h_category_id' value="<?php echo $i_category_id; ?>"/>
                </div>
                <script type="text/javascript" language="javascript">
                    function moveConfirm() {
                        //漫画名称
                        if (document.upform.i_book_name.value == "") {
                            alert("漫画名称を入力してください。");
                            document.upform.i_book_name.focus();
                            return false;
                        }
                        //フォルダー名
                        if (document.upform.i_folder_name.value == "") {
                            alert("フォルダー名を選択してください。");
                            document.upform.i_folder_name.focus();
                            return false;
                        }

                        var val_cat = $("#i_category_id").multiselect("getChecked").map(function() {
                            return this.value;
                        }).get();
                        document.upform.h_category_id.value = val_cat;
                        //submit
                        document.upform.action = "?action=insert";
                        document.upform.submit();
                    }
                </script>
            </form>
        </div>
    </body>
</html>
