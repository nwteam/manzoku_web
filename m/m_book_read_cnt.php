<?php
	//include
	require '../util/include.php';
	$sub_title='漫画読み回数';
	$systime=date('Y-m-d H:i:s',time());
	$sys_year=date('Y',time());
	$sys_month=date('m',time());
	$searchDateTo=date('Y-m',strtotime("+1 day"));

    $role=$_SESSION['role'];
    $login_user=$_SESSION['login_user'];

	$action = $_GET['action'];

	//Search
	if ($action=='search'){

		$db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		mysqli_set_charset($db,'utf8');

		$page_size=100;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;
		//FORM項目
		$s_book_id=$_POST['s_book_id'];
		if($_GET['s_book_id']!='') {
			$s_book_id=$_GET['s_book_id'];
		}
		$s_book_name=$_POST['s_book_name'];
		if($_GET['s_book_name']!='') {
			$s_book_name=$_GET['s_book_name'];
		}
		$i_supply_time_from=$_POST['i_supply_time_from'];
		if($_GET['i_supply_time_from']!='') {
			$i_supply_time_from=$_GET['i_supply_time_from'];
		}
		$i_supply_time_to=$_POST['i_supply_time_to'];
		if($_GET['i_supply_time_to']!='') {
			$i_supply_time_to=$_GET['i_supply_time_to'];
		}

		//All
		$sqlall = "select mbrc.*,
					mb.book_name,
					mb.book_auth 
					from mz_book_read_cnt mbrc inner join mz_book mb on mbrc.book_id=mb.book_id WHERE 1";
		$sql_count_month="select year,month,SUM(read_cnt) read_cnt_month from (".$sqlall.") A group by year,month";
		$sql_count_all="select SUM(mbrc.read_cnt) read_cnt_all from mz_book_read_cnt mbrc inner join mz_book mb on mbrc.book_id=mb.book_id WHERE 1";
        if($s_book_id!='') {
            $sqlall .= " and mbrc.book_id = '".$s_book_id."'";
        }
		if($s_book_name!='') {
            $sqlall .= " and mb.book_name like '%".$s_book_name."%'";
        }
		if ($i_supply_time_from!='' && $i_supply_time_to!='') {
               //$sqlall .= " and mbrc.year between ".substr($i_supply_time_from,0,4)." and ".substr($i_supply_time_to,0,4);
               //$sqlall .= " and mbrc.month between ".substr($i_supply_time_from,-1,2)." and ".substr($i_supply_time_to,-1,2);
               $y_from = date('Y', strtotime($i_supply_time_from."-01"));
               $m_from = date('m', strtotime($i_supply_time_from."-01"));
               $y_to = date('Y', strtotime($i_supply_time_to."-01"));
               $m_to = date('m', strtotime($i_supply_time_to."-01"));
               $sqlall .= " and mbrc.year between {$y_from} and {$y_to}";
               $sqlall .= " and mbrc.month between {$m_from} and {$m_to}";
        }
		
		$result = mysqli_query($db,$sqlall) or die(mysqli_error($db));
		$result_month = mysqli_query($db,$sql_count_month) or die(mysqli_error($db));
		$result_all = mysqli_query($db,$sql_count_all) or die(mysqli_error($db));

		if(!$result){
			$rowCnt = -1;
			mysqli_close($db);
		}
		$rowCntall=mysqli_num_rows($result);

		//Select current all
		$sql = sprintf("%s order by mbrc.read_cnt desc limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

		$result = mysqli_query($db,$sql);

		if(!$result){
			$rowCnt = -1;
			mysqli_close($db);
		}

		$rowCnt=mysqli_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			mysqli_close($db);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
		   $page_string .= '<a href=?action=search&page=1&s_book_id='.$s_book_id.'&s_book_name='.$s_book_name.'&i_supply_time_from='.$i_supply_time_from.'&i_supply_time_to='.$i_supply_time_to.'>トップページ</a>|<a href=?action=search&page='.($page-1).'&s_book_id='.$s_book_id.'&s_book_name='.$s_book_name.'&i_supply_time_from='.$i_supply_time_from.'&i_supply_time_to='.$i_supply_time_to.'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
		   $page_string .= '<a href=?action=search&page='.($page+1).'&s_book_id='.$s_book_id.'&s_book_name='.$s_book_name.'&i_supply_time_from='.$i_supply_time_from.'&i_supply_time_to='.$i_supply_time_to.'>次頁</a>|<a href=?action=search&page='.$page_count.'&s_book_id='.$s_book_id.'&s_book_name='.$s_book_name.'&i_supply_time_from='.$i_supply_time_from.'&i_supply_time_to='.$i_supply_time_to.'>最終ページ</a>';
		}
	}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="../js/datepicker.js"></script>
<script>
$(document).ready(function(){
	$('.i_supply_time_from').DatePicker({
		format:'Y-m',
		date: $('#i_supply_time_from').val(),
		current: $('#i_supply_time_from').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			$('#i_supply_time_from').DatePickerSetDate($('#i_supply_time_from').val(), true);
		},
		onChange: function(formated, dates){
			$('#i_supply_time_from').val(formated);
			$('#i_supply_time_from').DatePickerHide();
		}
	});
	$('.i_supply_time_to').DatePicker({
		format:'Y-m',
		date: $('#i_supply_time_to').val(),
		current: $('#i_supply_time_to').val(),
		starts: 1,
		position: 'right',
		onBeforeShow: function(){
			$('#i_supply_time_to').DatePickerSetDate($('#i_supply_time_to').val(), true);
		},
		onChange: function(formated, dates){
			$('#i_supply_time_to').val(formated);
			$('#i_supply_time_to').DatePickerHide();
		}
	});
});
</script>
<link href="../css/datepicker.css" type="text/css" rel="stylesheet">
<style type="text/css">
input.i_supply_time_from{border:1px solid #999;padding:4px;border-bottom-color:#ddd;border-right-color:#ddd;width:100px;}
input.i_supply_time_to{border:1px solid #999;padding:4px;border-bottom-color:#ddd;border-right-color:#ddd;width:100px;}
</style>
</head>
<body>
<?php
	//累計読む回数：
	$rs_all=mysqli_fetch_object($result_all);
	$read_cnt_all=$rs_all->read_cnt_all;
	//年月単位読む回数：
	while($rs_month=mysqli_fetch_object($result_month)){
		if($rs_month->year==$sys_year&&$rs_month->month==$sys_month){
			$read_cnt_month=$rs_month->read_cnt_month;
		}
	}
?>	
<div class='main'>
<div class='subtitle'><div class='ml20 fl'><?php echo $sub_title; ?></div><div class='mr20 fr'>累計読む回数：<?php echo $read_cnt_all; ?>回</div><div class='mr20 fr'><?php echo $sys_year; ?>年<?php echo $sys_month; ?>月読む回数：<?php echo $read_cnt_month; ?>回</div></div>
<form action='?action=search' method='post' name='form1'>
<div class='input-area'>
    <label class='search_label w100'>ブックID</label>
	<input type='text' name='s_book_id' id='s_book_id' class='w200' value='<?php echo $s_book_id;?>'/>
    <div style='clear:both;'></div>
    <label class='search_label w100'>ブック名称</label>
	<input type='text' name='s_book_name' id='s_book_name' class='w500' value='<?php echo $s_book_name;?>'/>
    <div style='clear:both;'></div>
    <label class='search_label w100'>年月</label>
	<input type='text' name='i_supply_time_from' id='i_supply_time_from' class='i_supply_time_from w100' value='<?php if($i_supply_time_from==''){echo '2015-01';}else{echo $i_supply_time_from;}?>'/>
	<label>〜</label>
	<input type='text' name='i_supply_time_to' id='i_supply_time_to' class='i_supply_time_to w100' value='<?php if($i_supply_time_to==''){echo $searchDateTo;}else{echo $i_supply_time_to;};?>'/>
    <input type='submit' class='buttonS bGreen ml100' value='絞り込み'/>
</div>
<?php
if ($rowCnt>0){
	echo "
		<table width='98%' cellspacing='1' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	echo "
		<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
			  <th width='100px'>年月</th>
			  <th width='100px'>読み回数</th>
              <th width='100px'>ブックID</th>
              <th width='240px'>漫画名</th>
              <th width='240px'>作者名</th>
			</tr>
		</table>
	";
	$i=1;
	while($rs=mysqli_fetch_object($result))
	{
	  echo "
			<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
				<tr align='left' bgcolor='#EEF2F4'>
					<td width='100px'align='center'>".$rs->year.'年'.$rs->month.'月'."</td>
					<td width='100px'align='center'>".$rs->read_cnt.'回'."</td>
					<td width='100px'align='center'>".$rs->book_id."</td>
					<td width='240px'align='center'>".$rs->book_name."</td>
					<td width='240px'align='center'>".$rs->book_auth."</td>
                    </tr>
                </table>
		    ";
        
		$i++;
	}
	echo "
		<table width='98%' cellspacing='1' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
			  <td><span style='float:left; text-align:center;font-size:16px;'><font color=#666666>$page_string</font></span></td>
			</tr>
		</table>";
	mysqli_close($db);
}else{
	if ($action=='search'){
		echo "検索結果がありません。";
	}
}
?>
</form>
</div>
</body>
</html>