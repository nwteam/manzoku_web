<?php
	//include
	require '../util/include.php';
    session_start();
	$sub_title='漫画管理　- 表紙イメージファイルチェックツール -';

	$db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	mysqli_set_charset($db,'utf8');
	$sql = "select * from mz_book WHERE 1 order by book_id";
	$result = mysqli_query($db,$sql);
	$rowCnt=mysqli_num_rows($result);
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $sub_title; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/common.js"></script>
</head>
<body>
<div class='main'>
<div class='subtitle'><?php echo $sub_title; ?></div>
<form action='?action=search' method='post' name='form1'>
<?php
if ($rowCnt>0){
	echo "
		<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
              <th width='100px'>ブックID</th>
              <th width='240px'>漫画名</th>
              <th width='100px'>フォルダー名</th>
              <th width='240px'>表紙パス</th>
              <th width='60px'>存在ﾁｪｯｸ</th>
              
			</tr>
		</table>
	";
	while($rs=mysqli_fetch_object($result))
	{
	  	$cover_path='../'.$rs->cover_img_path;
		if(file_exists($cover_path)){
		}else{
	  	echo "
			<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
				<tr align='left' bgcolor='#EEF2F4'>
					<td width='100px'align='center'>".$rs->book_id."</td>
					<td width='240px'>".$rs->book_name."</td>
					<td width='100px'>".$rs->folder_name."</td>
					<td width='240px'>".$rs->cover_img_path."</td>
					<td width='60px'align='center' style='background-color:red;'>パスNG</td>
				</tr>
            </table>
		";
		}
	}
	mysqli_close($db);
}else{echo '全体OKになりました！';}
?>
</form>
</div>
</body>
</html>