<?php
session_start();
//include
require '../util/include.php';

if (empty($_SESSION['login_user'])) {
    header('Location: index.php');
} else {
    $login_user = $_SESSION['login_user'];
    $role = $_SESSION['role'];
}
?>
<!DOCTYPE HTML>
<html xmlns=http://www.w3.org/1999/xhtml>
    <head>
        <meta http-equiv='content-type' content='text/html; charset=utf-8' />
        <meta http-equiv='content-style-type' content='text/css' />
        <meta http-equiv='content-script-type' content='text/javascript' />
        <link href='../css/common.css' type='text/css' rel='stylesheet' />
    </head>
    <body>
        <div class='slidebar'>
            <ul>
                <li><a href='m_comic.php?action=search' target ='maincontent'>漫画管理</a></li>
                <li><a href='m_book_read_cnt.php?action=search' target ='maincontent'>読み回数(ブック)</a></li>
                <!--<li><a href='m_chapter_read_cnt.php' target ='maincontent'>読み回数(巻)</a></li>-->
                <li><a href='m_category.php?action=search' target ='maincontent'>ジャンル管理</a></li>
                <li><a href='m_ad_flg.php?action=search' target ='maincontent'>広告管理</a></li>
                <li><a href='m_ad_click.php' target ='maincontent'>広告Click回数DL</a></li>
            </ul>
        </div>
    </body>
</html>