<?php
	//include
	require '../util/include.php';
	session_start();

	if(empty($_SESSION['login_user']))
	{
		header('Location: index.php');
	}else{
		$login_user=$_SESSION['login_user'];
	}

	$sysdate=date('Y-m-d',time());
	$systime_from=strtotime($sysdate.' 00:00:00');
	$systime_to=strtotime($sysdate.' 23:59:59');

?>
<!DOCTYPE HTML>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<meta http-equiv='content-type' content='text/html; charset=utf-8' >
<meta http-equiv='content-style-type' content='text/css'>
<meta http-equiv='content-script-type' content='text/javascript'>
<link href='../css/common.css' type='text/css' rel='stylesheet'>
</head>
<body>
		<div class='main'>
			<div class='mainContent clearfix'>
			</div>
		</div>
</body>
</html>