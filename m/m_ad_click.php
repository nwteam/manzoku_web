<?php
//ディレクトリ・ハンドルをオープン
$url = "/ad_data/five_any/native_x_2/log";
$dirPath = dirname(dirname(__FILE__)) . $url;
$resDir = opendir($dirPath);

$csvFiles = array();
while ($fileName = readdir($resDir)) {

    if (strstr($fileName, '.csv') !== false) {//CSVか？
        $csvFiles[] = $fileName;
    }
}
closedir($resDir);
?>

<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
    <head>
        <title>広告クリック回数のダウンロード</title>
        <meta http-equiv="content-type" content="text / html; charset = utf-8" />
        <style type="text/css">
            body{
                padding:0 10px 10px;
            }
            h2{
                margin:30px 0 5px;
                padding:0 5px 0;
                line-height:1.0em;
                font-size:20px;
                border-left:solid 5px #000; 
            }
            ul, li{
                margin:0px;
                padding:0px;
                list-style:none;
                line-height:2.0em;
            }
        </style>
    </head>
    <body>
        <?php if (empty($csvFiles)) { ?>
            <p>データがありません。</p>
        <?php } else { ?>
            <h2>ネイティブ広告 / ファイブエニー</h2>
            <ul>
                <?php foreach ($csvFiles as $csvFile) { ?>
                    <li>
                        <a href="<?php echo $url ?>/<?php echo $csvFile ?>" title="<?php echo $csvFile ?>"><?php echo $csvFile ?></a>
                    </li>
                <?php } ?>
            </ul>
        <?php } ?>
    </body>
</html>