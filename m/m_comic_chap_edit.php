<?php
//include
require '../util/include.php';

$sub_title = '話管理';
$systime = date('Y-m-d H:i:s', time());

$role = $_SESSION['role'];
$login_user = $_SESSION['login_user'];

$action = $_GET['action'];
$book_id = $_GET['b_id'];
if ($book_id == '') {
    $book_id = $_POST['book_id'];
}
$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
if (!$db) {
    die("connot connect:" . mysqli_error($db));
}
$dns = mysqli_select_db($db, DB_NAME);
if (!$dns) {
    die("connot use db:" . mysqli_error($db));
}
mysqli_set_charset($db, 'utf8');

//カテゴリプルダウンリスト取得
$sqlall = "select * from mz_category WHERE 1 and del_flg=0";
$result_list_cate = mysqli_query($db, $sqlall);
$sqlall = "select * from mz_book WHERE book_id='" . $book_id . "'";
$result_book = mysqli_query($db, $sqlall);
$rs_book = mysqli_fetch_object($result_book);

$folder_name = $rs_book->folder_name;
$folder_path = 'comic/' . $folder_name;
$folder_path_nodot = 'comic/' . $folder_name;
$chap_cnt = sizeof(myScandir($folder_path, true));

mysqli_close($db);

//一括更新
if ($action == 'updateall') {
    $array_id = $_POST["u_id"];
    $array_page_cnt = $_POST["page_cnt"];
    $array_sub_title = $_POST["i_sub_title"];
    $array_chapter_name = $_POST["i_chapter_name"];
    $array_cover_img_path = $_POST["cover_img_path"];
    $array_on_line_date = $_POST["i_on_line_date"];
    $page_folder_name = $_POST["folder_name"];

    $logstr .= "$systime $ip INFO：▼book_id=$book_id話情報登録/更新開始 \r\n";
    //error_log($logstr, 3, '../log/gen.log');

    $db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if (!$db) {
        die("connot connect:" . mysqli_error($db));
    }
    $dns = mysqli_select_db($db, DB_NAME);
    if (!$dns) {
        die("connot use db:" . mysqli_error($db));
    }
    mysqli_set_charset($db, 'utf8');

    $sqlall = "select * from mz_chapter WHERE 1";
    $sqlall .= " and book_id = '" . $book_id . "'";

    $result = mysqli_query($db, $sqlall) or die(mysqli_error($db));

    if (!$result) {
        $rowCnt = -1;
        mysqli_close($db);
    }
    $rowCntall = mysqli_num_rows($result);

    for ($i = 0; $i < count($array_id); $i++) {
        $u_id = $array_id[$i];
        $page_cnt = $array_page_cnt[$i];
        $sub_title = $array_sub_title[$i];
        $chapter_name = $array_chapter_name[$i];
        $insert_time = strtotime($array_on_line_date[$i]);
        $update_time = strtotime($systime);

        $sql = sprintf("UPDATE mz_chapter SET sub_title ='%s',chapter_name ='%s',page_folder_name='%s',total_page=%d,insert_time=%d,update_time=%d WHERE id = %d", $sub_title, $chapter_name, $page_folder_name, $page_cnt, $insert_time, $update_time, $u_id);


        if (($i + 1) > $rowCntall) {
            $cover_img_path = $array_cover_img_path[($i - $rowCntall)];
            $sql = sprintf("insert into mz_chapter (book_id,chapter_id,sub_title,chapter_name,cover_img_path,page_folder_name,total_page,insert_time) 
													values ('%s',%d,'%s','%s','%s','%s',%d,%d)", $book_id, $i + 1, $sub_title, $chapter_name, $cover_img_path, $page_folder_name, $page_cnt, $insert_time);
        }
        //echo $sql.'<br>';
        $logstr = "$systime $ip INFO：話情報登録/更新 SQL文： " . $sql . "\r\n";
        //error_log($logstr, 3, '../log/gen.log');
        $result = mysqli_query($db, $sql);
        if (!$result) {
            mysqli_close($db);
            $logstr = "$systime ERR：話情報DB登録/更新異常！ \r\n";
            $logstr .= "$systime $ip INFO：▲話情報登録/更新異常終了 \r\n";
            error_log($logstr, 3, '../log/gen.log');

            $err_cd_list[] = "01";
            $_SESSION['err_cd_list'] = $err_cd_list;
            $url = URL_PATH . "err.php";
            redirect($url);
        }
    }

    mysqli_close($db);
    $logstr = "$systime $ip INFO：▲話情報登録正常終了！！ \r\n";
    //error_log($logstr, 3, '../log/gen.log');
    echo "<script>alert('話情報を正常に登録・更新しました。');</script>";
}
//Search
if ($action == 'insert' || $action == 'update' || $action == 'edit' || $action == 'updateall') {

    $db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    mysqli_set_charset($db, 'utf8');

    $page_size = 999;

    if (isset($_GET['page'])) {
        $page = intval($_GET['page']);
    } else {
        $page = 1;
    }
    $rowCnt = 0;

    //All
    $sqlall = "select * from mz_chapter WHERE 1";
    if ($book_id != '') {
        $sqlall .= " and book_id = '" . $book_id . "'";
    }

    $result = mysqli_query($db, $sqlall) or die(mysqli_error($db));

    if (!$result) {
        $rowCnt = -1;
        mysqli_close($db);
    }
    $rowCntall = mysqli_num_rows($result);

    //Select current all
    $sql = sprintf("%s order by chapter_id limit %d,%d", $sqlall, ($page - 1) * $page_size, $page_size);

    $result = mysqli_query($db, $sql);

    if (!$result) {
        $rowCnt = -1;
        mysqli_close($db);
    }

    $rowCnt = mysqli_num_rows($result);

    //paging
    if ($rowCnt == 0) {
        $page_count = 0;
        mysqli_close($db);
    } else {
        if ($rowCntall < $page_size) {
            $page_count = 1;
        }
        if ($rowCntall % $page_size) {
            $page_count = (int) ($rowCntall / $page_size) + 1;
        } else {
            $page_count = $rowCntall / $page_size;
        }
    }
    $page_string = '';
    if (($page == 1) || ($page_count == 1)) {
        $page_string .= 'トップページ|第<b>' . ($page) . '</b>頁|計<b>' . ($page_count) . '</b>頁|';
    } else {
        $page_string .= '<a href=?action=search&page=1&role=' . $role . '&l_id=' . $login_user . '&b_name=' . $s_book_name . '&c_id=' . $s_category_id . '>トップページ</a>|<a href=?action=search&page=' . ($page - 1) . '&role=' . $role . '&l_id=' . $login_user . '&b_name=' . $s_book_name . '&c_id=' . $s_category_id . '>前頁</a>|第<b>' . ($page) . '</b>頁|計<b>' . ($page_count) . '</b>頁|';
    }
    if (($page == $page_count) || ($page_count == 0)) {
        $page_string .= '次頁|最終ページ';
    } else {
        $page_string .= '<a href=?action=search&page=' . ($page + 1) . '&role=' . $role . '&l_id=' . $login_user . '&b_name=' . $s_book_name . '&c_id=' . $s_category_id . '>次頁</a>|<a href=?action=search&page=' . $page_count . '&role=' . $role . '&l_id=' . $login_user . '&b_name=' . $s_book_name . '&c_id=' . $s_category_id . '>最終ページ</a>';
    }
}
?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
    <head>
        <title><?php echo $sub_title; ?></title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="content-style-type" content="text/css"/>
        <meta http-equiv="content-script-type" content="text/javascript"/>
        <link href="../css/common.css" type="text/css" rel="stylesheet"/>
        <script type="text/javascript" src="../js/common.js"></script>
    </head>
    <body>
        <div class='main'>
            <div class='subtitle'><?php echo $rs_book->book_name; ?>(<?php echo $rs_book->book_id; ?>) - <?php echo $sub_title; ?></div>
            <form action='?action=search' method='post' name='form1'>
                <input type='button' class='buttonS bGreen ml5 mt3' value='戻る' onclick='backToList();'/>
                <input type='button' class='buttonS bGreen' value='一括更新' onclick='updateAll();'/>
                <?php
                echo "
		<input type='hidden' name='folder_name' id='folder_name' value='" . $rs_book->folder_name . "'>
		<input type='hidden' name='book_id' id='book_id' value='" . $rs_book->book_id . "'>
		<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
			<tr bgcolor='#DBE6F5'>
              <th width='30px'>話ID</th>
              <th width='40px'>話数名</th>
              <th width='200px'>話タイトル</th>
              <th width='100px'>パス</th>
              <th width='50px'>ページ数</th>
              <th width='80px'>オンライン<br>日付</th>
			</tr>
		</table>
	";

                $i = 1;
                while ($rs = mysqli_fetch_object($result)) {
                    $chap_folder_path = $folder_path . "/" . $i;
                    $page_cnt = getPageCnt($chap_folder_path, false);
                    echo "
			<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
				<tr align='left' bgcolor='#EEF2F4'>
					<input type='hidden' name='u_id[] id='u_id[]' value='" . $rs->id . "'>
					<input type='hidden' name='page_cnt[]' id='page_cnt[]' value='" . $page_cnt . "'>
					<td width='30px'align='center'>" . $rs->chapter_id . "</td>
					<td width='40px'><input type='text' style='font-size:16px;width:94%;' name='i_sub_title[]' id='i_sub_title[]' value='" . $rs->sub_title . "'></td>
					<td width='200px'><input type='text' style='font-size:16px;width:98%;' name='i_chapter_name[]' id='i_chapter_name[]' value='" . $rs->chapter_name . "'></td>
                    <td width='100px'align='center'>" . $rs_book->folder_name . "</td>
					<td width='50px'align='center'>" . $page_cnt . "</td>
					<td width='80px'align='center'><input type='text' style='font-size:16px;width:97%;text-align:center;' name='i_on_line_date[]' id='i_on_line_date[]' value='" . date('Y-m-d', $rs->insert_time) . "'></td>
                </tr>
            </table>
	";
                    $online_date = date('Y-m-d', $rs->insert_time);

                    $i++;
                }
                if ($chap_cnt >= $i) {
                    for ($j = $i; $j <= $chap_cnt; $j++) {
                        $chap_folder_path = $folder_path . "/" . $j;
                        $cover_img_path = getFirstPagePath($chap_folder_path, false);
                        $page_cnt = getPageCnt($chap_folder_path, false);
                        $next_online_date = date('Y-m-d', strtotime("$online_date + 1 day"));
                        echo "
			<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
				<tr align='left' bgcolor='#EEF2F4'>
					<input type='hidden' name='u_id[]' id='u_id[]' value='" . $j . "'>
					<input type='hidden' name='page_cnt[]' id='page_cnt[]' value='" . $page_cnt . "'>
					<input type='hidden' name='cover_img_path[]' id='cover_img_path[]' value='" . $cover_img_path . "'>
					<td width='50px'align='center'>" . $j . "</td>
					<td width='40px'><input type='text' style='font-size:16px;width:94%;' name='i_sub_title[]' id='i_sub_title[]' value=''></td>
                    <td width='200px'><input type='text' style='font-size:16px;width:98%;' name='i_chapter_name[]' id='i_chapter_name[]' value=''></td>			
                    <td width='100px'align='center'>" . $rs_book->folder_name . "</td>
					<td width='50px'align='center'>" . $page_cnt . "</td>
					<td width='80px'align='center'><input type='text' style='font-size:16px;width:97%;text-align:center;' name='i_on_line_date[]' id='i_on_line_date[]' value='" . $next_online_date . "'></td>
                </tr>
            </table>
			";
                        $online_date = $next_online_date;
                    }
                }
                ?>
            </form>
            <script language="javascript" type="text/javascript">
                function backToList() {
                    var pageurl = "m_comic.php?action=search";
                    window.location.href = pageurl;
                }
                function updateAll() {
                    document.form1.action = '?action=updateall';
                    document.form1.submit();
                }
            </script>
        </div>
    </body>
</html>
