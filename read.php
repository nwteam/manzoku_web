<?php
session_start();
require 'util/include.php';
$detect = new Mobile_Detect();
if ($detect->isMobile() || $detect->isTablet()) {
    
} else {
    redirect(LP_PATH . 'pc.html');
}

$sysdate = date('Y-m-d', time());
$systime = date('Y-m-d H:i:s', time());

$login_id = $_SESSION['login_id'];

$page = $_GET['page']; //初回表示ページ
$folder = $_GET['folder']; //漫画データ(jpg)の格納フォルダ
$total_page = $_GET['total_page']; //トータルページ数
$b_id = $_GET['b_id']; //ブックID
$b_name = $_GET['b_name']; //ブック名
$c_id = $_GET['c_id']; //チャプターID
$b_auth = $_GET['b_auth']; //著者

if ($total_page == '' || $total_page == '0') {
    $total_page = '1';
}

$page_dir = "comic/{$folder}/{$c_id}";
$filesnames = array();
$tempArray = getS3FileList($page_dir);
$tempArray = array_reverse($tempArray);
foreach ($tempArray as $name) {
    if (($name != ".") AND ( $name != "..")) {
        $filesnames[] = COMIC_PATH."comic/{$folder}/{$c_id}/{$name}";
    }
}

$adPage = AD_PAGE;
$readPage = sizeof($filesnames); //ページ数取得
if ($readPage <= AD_PAGE) {
    $adPage = AD_PAGE - 5;
}
?>
<!doctype html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="keywords" content="マンガ,漫画,まんが,コミック,漫画アプリ,電子書籍,無料,全巻,Webコミック,Webマンガ,book,comic,BL,TL,ボーイズラブ,ティーンズラブ,人気マンガ,人気漫画,まんぞく,manzoku,漫ZOKU,漫zoku,マンゾク">
        <meta name="description" content="「漫ZOKU」とは全巻無料でマンガが読める電子書籍サービスです。話題のwebマンガから、人気漫画雑誌で連載していた不朽の名作まで、様々な種類のマンガを無料でお読みいただけます。">
        <meta name="viewport" content="width=device-width">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <title>漫ZOKU</title>
        <link rel="stylesheet" href="css/main.css">
        <script src="js/read/jquery-1.9.0.min.js" ></script>
        <script src="js/read/spin.js" ></script>
        <script src="js/jquery.min.js" ></script>
        <script src="js/jquery.leanModal.min.js" type="text/javascript"></script>

        <!-- レクタング広告用JS -->
        <script type="text/javascript">
            var loadImage_len;
            $(document).ready(function() {
                $('a[rel*=leanModal]').leanModal({top: 100, closeButton: ".modal_close"});
                $('#OpenAdPop').trigger("click"); //初回のレクタングル広告

                var imageload_count;
                var assets_url = [];
                var loadimg_W = [];
                var loadimg_H = [];

                //要素(htmlタグ)を変数に保存
                var progressBox = $('#progress-box').css({'display': 'none'});
                var progressBar = $('#progress-bar');
                var progressMessage = $('#progress-message').css({'display': 'none'});
                var readBtn = $('#read_btn');
                var mainContent = $('#main-content');

                //読み込み画像一覧を定義
                var imagelist = [
<?php foreach ($filesnames as $name) { ?>
                        "<?php echo $name ?>",
<?php } ?>
                ];
                loadImage_len = imagelist.length;

                //画像の一括読み込み処理
                bulkload();

                //画像の一括読み込み＆ブラウザキャッシュへ保存
                function bulkload() {
                    progress(0);
                    imageload_count = 0;
                    multiLoader();
                }

                //画像の読み込み（再帰処理の管理）
                function multiLoader() {

                    //ProgressBar描画
                    progress(imageload_count / loadImage_len);

                    //画像Load終了判定
                    if (imageload_count < loadImage_len) {
                        getLoadImage();//読み込み中
                    } else {
                        multicomplete();//読み込みが全て終了
                    }
                }

                //画像の読み込み（個別処理）
                function getLoadImage() {
                    var image_loader = new Image();

                    //読み込み完了時の処理
                    image_loader.onload = function() {

                        //サムネール大きさ取得
                        loadimg_W.push(image_loader.width);
                        loadimg_H.push(image_loader.height);

                        //サムネールURL保存
                        assets_url.push(imagelist[imageload_count]);

                        //次画像のロード（再帰）
                        imageload_count++;
                        multiLoader();
                    }

                    //読み込みエラー時の処理
                    image_loader.onerror = function() {
                        alert("ダウンロードに失敗しました");
                        return;
                    }
                    image_loader.src = imagelist[imageload_count];
                }

                //全画像読み込み完了時の処理
                function multicomplete() {
                    progress(1);

                    //プログレスバーの削除
                    setTimeout(function() {
                        progressBox.css({'display': 'none'});
                        progressMessage.css({'display': 'none'});
                        mainContent.css({'display': 'block'});
                        readBtn.css({'display': 'block'});
                    }, 1000);
                }

                //プログレスバーの表示
                function progress(progress_v) {
                    progressBox.css({'display': 'block'});
                    progressBar.css({'width': progress_v * 100 + '%'});
                    progressMessage.css({'display': 'block'});
                }
            });

            //デザイン変更の為にCSS更新
            function changeCss(fromId, toId) {
                if (fromId != toId) {
                    obj = document.getElementById(fromId);
                    obj.id = toId;
                }
            }
        </script>

        <style>
            .swipe {overflow: hidden;visibility: hidden;position: relative;}
            .swipe-wrap {overflow:hidden;position:relative;}
            .swipe-wrap > figure {float: left;width: 100%;position: relative;}
            .tool {background:#fff;position:absolute;bottom:0;z-index:9999;height:20px;height: 40px;display:none;}
            .tool span{display:block;width:20%;float:right;line-height:40px;font-family:'microsoft yahei';}            
            .tool input[type="range"]{margin-top:15px;float:left;width:80%;-webkit-appearance:none;width:70%;margin-left:25px;border-radius:4px;background-color:#DBDBDB;height:10px;}
            .tool input[type="range"]::-webkit-slider-thumb{-webkit-appearance:none;cursor:default;top:1px;height:20px;width:20px;background:none repeat scroll 0 0 #eee;border-radius:10px;border:1px solid #bbb;}
            #lean_overlay { position: fixed; z-index: 100; top: 0px; left: 0px; height: 100%; width: 100%; background: #000; display: none; }
            #read_btn {
                position: relative;
                background:#FFF;
                width:300px;
                line-height:2em;
                height:2em;
                text-align: center;
                color:#333;
                font-weight: bold;
                border:solid 1px #CCC;
                margin:5px 0 0;
                -webkit-border-top-left-radius: 5px;  
                -webkit-border-top-right-radius: 5px;  
                -webkit-border-bottom-right-radius: 5px;  
                -webkit-border-bottom-left-radius: 5px;  
                -moz-border-radius-topleft: 5px;  
                -moz-border-radius-topright: 5px;  
                -moz-border-radius-bottomright: 5px;  
                -moz-border-radius-bottomleft: 5px;
                font-family:'Meiryo UI', 'メイリオ', 'ヒラギノ角ゴ ProN W3', 'Hiragino Kaku Gothic ProN', 'ＭＳ Ｐゴシック', sans-serif;
            }
            a#read_btn{
                color:#333;
                text-decoration: none;
            }
            a#close_btn{
                position: absolute;
                top:236px;
                left:0px;
                text-indent: -999px;
                width:14px;
                height:14px;
                background:url('images/modal_close.png') no-repeat;
            }
            .swipe {
                overflow: hidden;
                visibility: hidden;
                position: relative;
            }
            .swipe-wrap {
                overflow: hidden;
                position: relative;
            }
            .swipe-wrap > figure {
                float: left;
                width: 100%;
                position: relative;
            }
            #progress-box{
                width: 300px;
                height:15px;
                margin:5px 0 0;
                padding:5px;
                background:#EFEFEF;
                -webkit-border-top-left-radius: 5px;  
                -webkit-border-top-right-radius: 5px;  
                -webkit-border-bottom-right-radius: 5px;  
                -webkit-border-bottom-left-radius: 5px;  
                -moz-border-radius-topleft: 5px;  
                -moz-border-radius-topright: 5px;  
                -moz-border-radius-bottomright: 5px;  
                -moz-border-radius-bottomleft: 5px;  
            }
            #progress-bar{
                width:100%;
                height:5px;
                margin:0;
                padding:0;
                background-color:orange;
            }
            #progress-message{
                margin-top:5px;
                text-align:center;
                width: 300px;
                color:white;
                font-family:'Meiryo UI', 'メイリオ', 'ヒラギノ角ゴ ProN W3', 'Hiragino Kaku Gothic ProN', 'ＭＳ Ｐゴシック', sans-serif;
            }
        </style>
    </head>
    <body class="page-swipe">
        <header>
            <div id="slider" class="swipe" style="visibility: visible;">
                <div id="main-content" class="swipe-wrap" style="display:none;">
                    <?php foreach ($filesnames as $name) { ?>
                        <figure>
                            <div class="wrap">
                                <div class="image" style="background:url('<?php echo $name ?>') center no-repeat;background-size:auto 90%;"> </div>
                            </div>
                        </figure>
                    <?php } ?>
                </div>
            </div>
            <div class="tool">
                <input type="range" step="1" id="rChange" value="-1" max="-1" min="-<?php echo $total_page; ?>"/>
                <span><c>1</c>/<?php echo $total_page; ?><span/>
            </div>
        </header>
        <script src="js/qzwlib.js"></script>
        <script>
            var b_id = "<?php echo $b_id; ?>", b_name = "<?php echo $b_name; ?>", c_id = "<?php echo $c_id; ?>", b_auth = "<?php echo $b_auth; ?>";
            var per_pos;
            //cookie exp date
            var date = new Date(), expiresDays = 1000;
            date.setTime(date.getTime() + expiresDays * 24 * 3600 * 1000);

            var slider =
                    qzwSlider(document.getElementById('slider'), {
                        startSlide:<?php echo ($total_page - $page); ?>,
                        auto: 99999000, //自動読みなら遅延時間
                        callback: function(pos) { //ページ移動「開始」毎にコールされる
                            //コールバック
                        },
                        transitionEnd: function(pos) { //ページ移動「終了」毎にコールされる
                            c = $('header figure').size();
                            p = c - pos;
                            $('.tool c').html(p);
                            $('#rChange').val(-p);

                            //cookie
                            document.cookie = b_id + "=" + c_id + "=" + p + ";expires=" + date.toGMTString();

                            //google 
                            ga('send', 'event', 'ビューワー', 'スワイプ', b_name + '-' + c_id + '-' + p);

                            if (pos == 0) {
                                if (per_pos == pos) {
                                    //last page 
                                    var pageurl = "last.php?b_id=" + b_id + "&b_name=" + b_name + "&c_id=" + c_id + "&b_auth=" + b_auth;
                                    window.location.href = pageurl;
                                    return;
                                }
                            }
                            per_pos = pos;
                            if ((loadImage_len - pos) % <?php echo $adPage ?> == 0) { //余りが0になる度にレクタング広告を表示
                                $('#OpenAdPop').trigger("click");
                            }
                        }
                    });
            $(function() {
                var page = 0, w = $('header').width(), pos = 0, count = $('header figure').size();

                $('#rChange').change(function() {
                    page = $(this).val();
                    slider.slide(count - (-page), 300);
                });

                $('header').click(function(e) {
                    pos = slider.getPos();
                    if (e.pageX <= w / 3) {
                        if (pos == 0) {
                            if (per_pos == pos) {
                                //last page 
                                var pageurl = "last.php?b_id=" + b_id + "&b_name=" + b_name + "&c_id=" + c_id + "&b_auth=" + b_auth;
                                window.location.href = pageurl;
                                return;
                            }
                        } else {
                            page = count - pos;
                            //cookie
                            document.cookie = b_id + "=" + c_id + "=" + page + ";expires=" + date.toGMTString();
                            //google 
                            ga('send', 'event', 'ビューワー', '左タップ', b_name + '-' + c_id + '-' + page);
                            //ad pop
                            if (pos % <?php echo $adPage ?> == 0) { //余りが0になる度にレクタング広告を表示
                                //$('#OpenAdPop').trigger("click");
                            }
                            //次ページ（左へ）
                            slider.prev();
                        }
                        per_pos = pos;
                    }
                    ;
                    if (e.pageX >= w / 3 * 2) {
                        if (pos == count - 1) {
                            alert('最初のページです。');
                            return;
                        } else {
                            //前ページへ（右へ）
                            page = count - pos;
                            //cookie
                            document.cookie = b_id + "=" + c_id + "=" + page + ";expires=" + date.toGMTString();
                            //google 
                            ga('send', 'event', 'ビューワー', '右タップ', b_name + '-' + c_id + '-' + page);
                            //ad pop
                            if (pos % <?php echo $adPage ?> == 0) { //余りが0になる度にレクタング広告を表示
                                //$('#OpenAdPop').trigger("click");
                            }

                            //next page
                            slider.next();
                        }
                    }
                    ;
                    if (e.pageX > w / 3 && e.pageX < w / 3 * 2)
                        $('.tool').slideToggle();
                });
            });
        </script>

        <!-- レクタングル広告 -->
        <div style="display: none;" >
            <a id="OpenAdPop" href="#adpop" rel="leanModal"></a>
        </div>
        <div id="adpop" style="background: none repeat scroll 0 0 #FFFFFF; border-radius: 5px 5px 5px 5px;
             box-shadow: 0 0 4px rgba(0, 0, 0, 0.7); display: none; padding-bottom: 2px;
             opacity: 1; position:absolute;width:300px;height:250px;
             top: 50%;left:50%;margin-left:-150px;margin-height:-125px;z-index: 11000;">         

            <div style='margin: 0 auto; width: 300px;height:250px;'>
                <?php if ($_SESSION['ad_flg'] == '0') { ?>
                    <!-- ad tags Size: 300x250 ZoneId:1048295-->
                    <script type="text/javascript" src="http://js.speead.jp/t/048/295/a1048295.js"></script>
                <?php } else { ?>

                <?php } ?>

            </div>

            <!-- プログレスバー -->
            <div id="progress-box">
                <div id="progress-bar"></div>
            </div>
            <div id='progress-message'>漫画データをダウンロード中</div>

            <!-- 漫画を読むボタン -->
            <a href="#" id="read_btn" class="modal_close" style="display:none;" onclick="changeCss(this.id, 'close_btn');">漫画を読む</a>
        </div>
    </body>
    <?php include_once("analyticstracking.php") ?>
</html>