<?php
session_start();
require 'util/include.php';

$detect = new Mobile_Detect();
if ($detect->isMobile() || $detect->isTablet()) {
    
} else {
    redirect(LP_PATH . 'pc.html');
}

$sysdate = date('Y-m-d', time());
$systime = date('Y-m-d H:i:s', time());

$b_id = $_GET['b_id']; //ブックID
$b_name = $_GET['b_name']; //ブック名
$b_auth = $_GET['b_auth']; //著作者

$sql = "select book_overview from mz_book where book_id = '" . $b_id . "'";
$r_book = mysqli_query($link, $sql);
$row_book = mysqli_fetch_array($r_book);
$bookOverview = $row_book['book_overview'];

$sql = "select * from mz_chapter where book_id = '" . $b_id . "' order by chapter_id";
$r_chapter = mysqli_query($link, $sql);

$action = $_GET['action'];
//Update
if ($action == 'readCnt') {

    $c_id = $_GET['c_id']; //チャプターID
    $i = $_GET['i']; //話No.
    $year = date('Y', time()); //現在の年
    $month = date('m', time()); //現在の月

    $sql = sprintf("select count(*) cnt from mz_chapter_read_cnt where book_id = '%s' and chapter_id = %d and year='%s' and month='%s'", $b_id, $c_id, $year, $month);
    $result_cc = mysqli_query($link, $sql);
    $rs_cc = mysqli_fetch_object($result_cc);
    $cnt = $rs_cc->cnt;

    if ($cnt == '0') {
        $sql = sprintf("insert into mz_chapter_read_cnt (book_id,chapter_id,year,month,read_cnt) values 
						('%s',%d,'%s','%s',1)"
            , $b_id, $c_id, $year, $month);
        $result = mysqli_query($link, $sql);
    } else {
        $sql = sprintf("select read_cnt from mz_chapter_read_cnt where book_id = '%s' and chapter_id = %d and year='%s' and month='%s'", $b_id, $c_id, $year, $month);
        $result = mysqli_query($link, $sql);
        $rs = mysqli_fetch_object($result);
        $read_cnt = ($rs->read_cnt) + 1;

        $sql = sprintf("UPDATE mz_chapter_read_cnt SET read_cnt=%d WHERE book_id = '%s' and chapter_id = %d and year='%s' and month='%s'", $read_cnt, $b_id, $c_id, $year, $month);
        $result = mysqli_query($link, $sql);
    }

    $url = "chapter_read.php?b_id=" . $b_id . "&c_id=" . $c_id . "&b_name=" . urlencode($b_name) . "&b_auth=" . urlencode($b_auth);
    redirect($url);
}

//チャプター情報の取得
$chapters = array();
while ($row = mysqli_fetch_array($r_chapter)) {
    $chapters[] = $row;
}
?>
<!doctype html>
<html lang="ja">
    <head>
        <title>漫ZOKU</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta id="viewport" name="viewport" content="width=device-width,initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <meta content="yes" name="apple-mobile-web-app-capable">
        <meta content="black" name="apple-mobile-web-app-status-bar-style">
        <link rel="stylesheet" type="text/css" href="css/layout.css">
        <link rel="stylesheet" type="text/css" href="css/meerkat.css">
        <style>
            .view_body{ }
            .read_vol{
                text-align: center;
                position: absolute;
                bottom: 10px;
                right: 12px;
                line-height: 100%;
                padding: 3px 0;
                border: solid 1px #d3d3d3;
                border-radius: 3px;
                background-color: #fff;
                width: 48px;
            }
        </style> 
        <script type="text/javascript" src="js/iscroll.js"></script>
        <script type="text/javascript" src="js/util.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.meerkat.1.3.min.js"></script>
        <script type="text/javascript">
            $(function() {
                $('.meerkat').meerkat({
                    height: '50px',
                    width: '100%',
                    position: 'bottom',
                    close: '.close-meerkat',
                    dontShowAgain: '.dont-show',
                    animationIn: 'slide',
                    animationSpeed: 500,
                    removeCookie: '.reset'
                }).addClass('pos-bot');
            });</script>

        <!-- サムネイル画像の非同期表示用JS -->
        <script src="js/read/jquery.lazyload.js"></script>
        <script>
            //Lazy Loadを起動する
            window.onload = function() {
                $("img.lazy").lazyload({
                    effect: "fadeIn",
                    //effect_speed: 3000
                });
            }
        </script>

    </head>
    <body>
        <div class="float_header">
            <div class="login_wrapper"></div>
            <div class="header">
                <div class="logo_wrapper">
                    <a href="<?php echo LP_PATH ?>"><img src="images/logo.png" alt="漫ZOKU"></a><!--M-->
                    <div class="login" id="login"><i></i></div>
                </div>
            </div>

            <div class="back_title">
                <b><a href="<?php echo LP_PATH ?>"></a></b>
                <h2 id="title"><a href="<?php echo LP_PATH ?>"></a><span class="c_find"><?php echo $b_name; ?></span></h2>
            </div>
        </div>

        <div class="view_body" id="top_page_div">
            <div id="scroller">
                <div class="list_wrapper" id="list_wrapper">
                    <div id="list_new">

                        <!-- 広告 -->
                        <div style="margin: 0 auto; width: 320px;">
                            <?php if ($_SESSION['ad_flg'] == 0) { ?>

                            <?php } else { ?>

                            <?php } ?>
                        </div>

                        <?php if (!empty($bookOverview)) { ?>
                            <div class="overview">
                                <p><b>あらすじ</b></p>
                                <p><?php echo nl2br(htmlspecialchars($bookOverview)) ?></p>
                            </div>
                        <?php } ?>

                        <?php $i = 1; ?>
                        <?php foreach ($chapters as $chapter) { ?>
                            <?php if ($chapter['insert_time'] > time()) { ?>
                                <!-- 近日公開（読みボタン未開放）-->
                                <div class="item">
                                    <dl class='clearfix'>
                                        <dt>                                        
                                        <img class="lazy" data-original="<?php echo COMIC_PATH ?><?php echo $chapter['cover_img_path'] ?>" src="images/chapter_loading.gif">             
                                        </dt>
                                        <dd class='title' style="padding-top:10px;">
                                            <?php echo $chapter['sub_title'] ?><br />
                                            <b style="font-size:15px;"><?php echo $chapter['chapter_name'] ?></b>
                                        </dd>
                                        <dd class='description'><?php echo $b_auth ?></dd>
                                    </dl>
                                    <a  class='read_vol'><?php echo date('m/d', $chapter['insert_time']) ?><br />更新！<b></b></a>
                                </div>
                            <?php } else { ?>

                                <!-- 現在公開中 -->
                                <div class='item' onclick="readCnt('<?php echo $chapter['book_id'] ?>', '<?php echo $chapter['chapter_id'] ?>', '<?php echo $b_name ?>', '<?php echo $i ?>', '<?php echo $b_auth ?>')">
                                    <dl class='clearfix'>
                                        <dt> 
                                        <img class="lazy" data-original="<?php echo COMIC_PATH ?><?php echo $chapter['cover_img_path'] ?>" src="images/chapter_loading.gif" >    
                                        </dt>
                                        <dd class='title' style="padding-top:10px;">
                                            <?php echo $chapter['sub_title'] ?><br />
                                            <b style="font-size:15px;"><?php echo $chapter['chapter_name'] ?></b>
                                        </dd>
                                        <dd class='description'><?php echo $b_auth ?></dd>
                                    </dl>
                                    <a href='#' id="chapter_btn<?php echo $chapter['chapter_id'] ?>" onclick="readCnt('<?php echo $chapter['book_id'] ?>', '<?php echo $chapter['chapter_id'] ?>', '<?php echo $b_name ?>', '<?php echo $i ?>', '<?php echo $b_auth ?>')" class='read_vol read_start_btn'>読む</a>
                                </div>
                            <?php } ?>    

                            <?php if ($i % 4 == 0) { ?>
                                <!-- レクタングル広告 -->
                                <?php if (true) {?>
                                <?php //if (isset($_GET['adcheck'])) { //広告のテスト用フラグ?>
                                    <script language="javascript" type="text/javascript">var addeluxue_conf = {site:733237222,frame:26,width:300,height:250,color:["999999","FFFFFF","2200CC","F25D5D","671F28"],host:'adv.addeluxe.jp',ver:1.5};</script><script language="javascript" type="text/javascript" src="http://img.addeluxe.jp/js/iframe/adv.js" charset="utf-8"></script>
                                <?php } else { ?>
                                    <div style='margin: 0 auto; width: 300px;height:250px;'>

                                    </div>
                                <?php } ?>
                                <!--// レクタングル広告 -->
                            <?php } ?>
                            <?php $i++; ?>
                        <?php } ?>
                        <br /><br /><br /><br /><br /><br />

                        <!-- フッター広告 -->
                        <div class="meerkat">
                            <div class="adsense" >
                                <a href="#" class="close-meerkat">close</a>
                                <?php if ($_SESSION['ad_flg'] == 0) { ?>

                                <?php } else { ?>

                                <?php } ?>
                            </div>
                        </div>
                        <!--// フッター広告 -->
                    </div>
                </div>
            </div>
        </div>

        <script>
                                        function readCnt(b_id, c_id, b_name, i, b_auth) {
                                            //$('#chapter_btn' + c_id).css({'background-color': '#CCC'});
                                            var pageurl = "chapter.php?action=readCnt&b_id=" + b_id + "&c_id=" + c_id + "&b_name=" + b_name + "&i=" + i + "&b_auth=" + b_auth;
                                            window.location.href = pageurl;
                                        }
        </script>
    </body>
    <?php include_once("analyticstracking.php") ?>
</html>

