<?php
function quote_smart($link,$value,$strFlg)
{
	// 数値以外をクオートする
	if (!is_numeric($value) || $strFlg) {
		$value = "'" . mysqli_real_escape_string($link,$value) . "'";
	}
	return $value;
}
function db_conn(){

	$db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysqli_error());
	}

	$dns = mysqli_select_db($db,DB_NAME);

	if(!$dns){
		die("connot use db:" . mysqli_error());
	}


	return $db;
}
function db_disConn($result,$db){

	//mysqli_free_result($result);
	mysqli_close($db);

}
function chkMem($u_id,$pwd){
	$link = db_conn();
	mysqli_set_charset($link,'utf8');

	$rowCnt = 0;

	$sql = sprintf("SELECT * from mz_admin where lower(u_id)=%s and status=0",quote_smart($link,$u_id,true));
	$result = mysqli_query($link,$sql);
	
	if(!$result){
		$rowCnt = -1;
		db_disConn($result, $link);
		return $rowCnt;
	}

	$rowCnt=mysqli_num_rows($result);

	if($rowCnt==0){
		db_disConn($result, $link);
		return $rowCnt;
	}else{
		$row=mysqli_fetch_assoc($result);
		$dbPasswd=$row['pwd'];
		if(strcmp($dbPasswd, $pwd)==0){
			$rowCnt=$row;
		}else{
			$rowCnt=2;
		}
	}

	mysqli_free_result($result);
	mysqli_close($link);

	return $rowCnt;
}
function getRole($u_id){
    $link = db_conn();
    mysqli_set_charset($link,'utf8');

    $rowCnt = 0;

    $sql = sprintf("SELECT role from mz_admin where lower(u_id)=%s and status=0",quote_smart($link,$u_id,true));
    $result = mysqli_query($link,$sql);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        return $rowCnt;
    }

    $rowCnt=mysqli_num_rows($result);
    if($rowCnt==0){
        db_disConn($result, $link);
        return $rowCnt;
    }else{
        $row=mysqli_fetch_assoc($result);
        $rowCnt=$row['role'];
    }

    mysqli_free_result($result);
    mysqli_close($link);

    return $rowCnt;
}
function chkPath($path){
    $link = db_conn();
    mysqli_set_charset($link,'utf8');
	
    $rowCnt = 0;
	
    $sql = sprintf("SELECT * from mz_book where folder_name = '%s'",$path);
    $result = mysqli_query($link,$sql);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        return $rowCnt;
    }
	$rowCnt=mysqli_num_rows($result);
    mysqli_close($link);
    return $rowCnt;
}
?>