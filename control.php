<?php

require_once 'util/include.php';

//１ページ当たりの表示画像件数
define(_PAGE_MAX, 10);

//登録漫画を全て取得
//$sql = "SELECT * FROM mz_book ORDER BY sort_num > 0 DESC, sort_num ASC, id DESC";
$sql = "SELECT DISTINCT mz_book.*";
$sql .= " FROM mz_book FORCE INDEX(unique_book_id)";
$sql .= " INNER JOIN mz_chapter ON mz_book.book_id = mz_chapter.book_id";
$sql .= " WHERE mz_chapter.insert_time <= '" . time() . "'";
$sql .= " ORDER BY mz_book.sort_num > 0 DESC, mz_book.sort_num ASC, mz_book.id DESC";

$r_books = mysqli_query($link, $sql);
$chapters = array();
while ($row = mysqli_fetch_array($r_books)) {
    $chapters[] = $row;
}

//カテゴリプルダウンリスト取得
$sql = "SELECT * FROM mz_category FORCE INDEX(idx_del_flg) WHERE del_flg=0";
$result_list_cate = mysqli_query($link, $sql);

$cateNames = array();
while ($arr_list_row = mysqli_fetch_array($result_list_cate)) {
    $cateId = $arr_list_row['cat_id'];
    $cateNames[$cateId] = $arr_list_row['cat_name'];
}
$cnt = 1;
$page = 1;

$image_datas = array();
if (!empty($chapters)) {
    //$image_datas[ページNo(1〜)][表示No(0〜)]の取得
    foreach ($chapters as $chapter) {
        if ($cnt <= _PAGE_MAX) {
            $image_datas[$page][] = $chapter;
            $cnt++;
        } else {
            $page++;
            $cnt = 1;
        }
    }
}
$lastPage = sizeof($image_datas);
$chapterCounter = 0;

//$_REQUEST['PAGE_NUM']はページ番号。
if (!empty($_REQUEST['PAGE_NUM']) && !empty($image_datas[$_REQUEST['PAGE_NUM']])) {

    $imgHtml = "";
    $pageNo = $_REQUEST['PAGE_NUM'];

    if ($_SESSION['read_page_count'] == 0) {//最初のページか？
        $imgHtml .= "<input type='hidden' name='last_page' value='{$lastPage}'>";
    }

    foreach ($image_datas[$_REQUEST['PAGE_NUM']] as $chapter) {
        $pageUrl = "index.php?action=readCnt&b_id=" . $chapter['book_id'] . "&b_name=" . $chapter['book_name'] . "&b_auth=" . $chapter['book_auth'];
        $imgUrl = COMIC_PATH . $chapter['cover_img_path'];
        $bookName = $chapter['book_name'];
        $bookAuth = $chapter['book_auth'];
        $cateIds = explode(",", $chapter['cat_id']);

        $imgHtml .= "<ul class='list clearfix' style='float:left;'>"; //※このulタグが１つの画像
        $imgHtml .= "  <li class='book'>";
        $imgHtml .= "    <p class='thumb'>";
        $imgHtml .= "      <a href='{$pageUrl}'>";
        //【重要】画像の遅延読み込み（lazyload.js）するときはコメント
        //$imgHtml .= "        <img src='{$imgUrl}' alt='{$bookName}' />";
        $imgHtml .= "        <img class='lazy{$pageNo}' data-original='{$imgUrl}' src='images/chapter_loading.gif' alt='{$bookName}' /> ";
        $imgHtml .= "      </a>";
        $imgHtml .= "    </p>";
        $imgHtml .= "    <dl>";
        $imgHtml .= "      <dt class='title'>{$bookName}</dt>";
        $imgHtml .= "      <dd class='name'>{$bookAuth}</dd>";
        $imgHtml .= "    </dl>";

        if (!empty($cateIds)) {
            $imgHtml .= "    <ul class='clearfix'>";
            foreach ($cateIds as $cateId) {
                $cateName = $cateNames[$cateId];
                if (!empty($cateName)) {
                    $cateUrl = "search.php?action=category&cat_id={$cateId}&cat_name={$cateName}{$cateName}";
                    $imgHtml .= "      <li style='width:65px;'>";
                    $imgHtml .= "        <a href='{$cateUrl}'>{$cateName}</a>";
                    $imgHtml .= "      </li>";
                }
            }
            $imgHtml .= "    </ul>";
        }
        $imgHtml .= "  </li>";
        $imgHtml .= "</ul>";

        $chapterCounter++;

        //5段毎（10タイトル毎）にネイティブ広告表示
        if ($chapterCounter != 0 && $chapterCounter % 10 == 0) {
            include('ad_data/five_any/native_x_2/index.php');
            $imgHtml .= $nativeAdImgHtml;
        }
    }

    //【重要】
    //画像の遅延読み込み（lazyload.js）を利用するときは、
    //必ずheadタグ内で <script src="js/read/jquery.lazyload.js"></script> を記載すること。
    /**/
    $imgHtml .= "<script type='text/javascript'>";
    $imgHtml .= "  $(function() {";
    $imgHtml .= "    $('img.lazy{$pageNo}').lazyload({";
    $imgHtml .= "      effect: 'fadeIn',";
    //$imgHtml .= "      effect_speed: 3000";
    $imgHtml .= "      load: function(e){";
    //$imgHtml .= "          callback";
    $imgHtml .= "      }";
    $imgHtml .= "    });";
    $imgHtml .= "  });";
    $imgHtml .= "</script>";
    /**/
    echo $imgHtml;
    $_SESSION['read_page_count'] ++;
} else {
    if ($_SESSION['read_page_count'] == $lastPage) {//ページは全て読み込んだか？
        $_SESSION['read_page_count'] ++;
        $movHtml = "<div style='clear:both;'><hr /></div>";
        $movHtml .= "<br /><br /><br /><br />";
        echo $movHtml;
        require_once 'ad_data/movie/index.php';
    }
}
?>

