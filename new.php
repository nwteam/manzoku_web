<?php
require_once 'util/include.php';

//新着漫画を１０件取得
//$sql = "select DISTINCT b.* from mz_book b, mz_chapter c where b.book_id=c.book_id and c.insert_time<='" . time() . "' order by c.insert_time desc limit 0,10";
$sql = "SELECT DISTINCT mz_book.*";
$sql .= " FROM mz_book FORCE INDEX(unique_book_id)";
$sql .= " INNER JOIN mz_chapter ON mz_book.book_id = mz_chapter.book_id";
$sql .= " WHERE mz_chapter.insert_time <= '" . time() . "'";
$sql .= " ORDER BY mz_book.insert_time DESC";
$sql .= " LIMIT 10;";

$r_new_books = mysqli_query($link, $sql);
$sqlTraceMessage .= "{$sql}\r\n";

$nowMtime = microtime(true) - $startMTime;
$traceMessage .= "[{$nowMtime}]sec 新着漫画を１０件取得\r\n";

//カテゴリプルダウンリスト取得
$sql = "SELECT * FROM mz_category FORCE INDEX(idx_del_flg) WHERE del_flg=0";
$result_list_cate = mysqli_query($link, $sql);

$cateNames = array();
while ($arr_list_row = mysqli_fetch_array($result_list_cate)) {
    $cateId = $arr_list_row['cat_id'];
    $cateNames[$cateId] = $arr_list_row['cat_name'];
}
$nowMtime = microtime(true) - $startMTime;
$traceMessage .= "[{$nowMtime}]sec カテゴリプルダウンリスト取得\r\n";

$nowMtime = microtime(true) - $startMTime;
$traceMessage .= "[{$nowMtime}]sec 新着のチャプターを取得開始\r\n";

//新着のチャプターを取得
$newChapters = array();
while ($row = mysqli_fetch_array($r_new_books)) {
    $newChapters[] = $row;
}
?>
<section id="new-contents">
    <h2>新着マンガ</h2>
    <div style="margin-left: 5%;">もっと新着を見たい方は<a href="search.php?action=new" style="color: #190AED; font-size: 20px;">コチラ</a></div>

    <!-- ポインタ -->
    <div class="pointer">
        <?php $count = 0; ?>
        <?php foreach ($newChapters as $chapter) { ?>
            <?php if (++$count == 1) { ?>
                <span class="current"></span>
            <?php } else { ?>
                <span></span>
            <?php } ?>
        <?php } ?>
    </div>
    <!--// ポインタ -->

    <!-- 画像 -->
    <div class="viewport">
        <div class="flipsnap">
            <?php foreach ($newChapters as $chapter) { ?>
                <?php $newPageUrl = "index.php?action=readCnt&b_id=" . $chapter['book_id'] . "&b_name=" . urldecode($chapter['book_name']) . "&b_auth=" . urlencode($chapter['book_auth']); ?>
                <?php $newImgUrl = COMIC_PATH . $chapter['cover_img_path']; ?>
                <?php $newBookName = $chapter['book_name'] ?>
                <?php $newBookAuth = $chapter['book_auth'] ?>
                <div class="item_group">
                    <div class="item">
                        <a href='<?php echo $newPageUrl ?>'>
                            <img src='<?php echo $newImgUrl ?>' alt='<?php echo $newBookName ?>' />
                            <dl>
                                <dt class='title'><?php echo $newBookName ?></dt>
                                <dd class='name'><?php echo $newBookAuth ?></dd>
                            </dl>
                        </a>
                    </div>
                    <ul class='category'>
                        <?php
                        /* ジャンル */
                        $cateIds = explode(",", $chapter['cat_id']);
                        if (!empty($cateIds)) {
                            foreach ($cateIds as $cateId) {
                                ?>  
                                <li>
                                    <a href='search.php?action=category&cat_id=<?php echo $cateId ?>&cat_name=<?php echo urlencode($cateNames[$cateId]) ?>'><?php echo $cateNames[$cateId] ?></a>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            <?php } ?>
        </div>
    </div>
    <!--// 画像 -->


</section>
