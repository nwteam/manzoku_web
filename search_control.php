<?php

require_once 'util/include.php';

//１ページ当たりの表示画像件数
define(_PAGE_MAX, 10);


$action = $_GET['action'];
$searchAction = "";

if ($action == '') {
    exit();
} else if ($action == 'list') {

    /**
     * マンガ一覧
     */
    $searchAction = "list";
    $searchBox = $_GET['searchBox'];
    $sql = sprintf("SELECT * FROM mz_book WHERE 1");
    if ($searchBox != '') {
        $sql .= " AND book_name collate utf8_unicode_ci LIKE '%" . ($searchBox) . "%'";
    }
    $sql .= " ORDER BY book_id ";
} else if ($action == 'new') {

    /**
     * 新着検索
     */
    $searchAction = "new";
    $searchBox = $_GET['searchBox'];
    $sql = sprintf("SELECT * FROM mz_book WHERE insert_time < %d ", time());
    if ($searchBox != '') {
        $sql .= " AND book_name collate utf8_unicode_ci LIKE '%" . ($searchBox) . "%'";
    }
    $sql .= "ORDER BY book_id DESC";
} else if ($action == 'category') {

    /**
     * カテゴリ検索
     */
    $searchAction = "category";
    $cat_id = $_GET['cat_id'];
    $cat_name = $_GET['cat_name'];
    $b_auth = $_GET['b_auth'];
    $searchBox = $_GET['searchBox'];
    $sql = sprintf("SELECT * FROM mz_book WHERE 1");

    if ($cat_id != '') {
        if ($cat_id == 'CT999') {//男性向けか？
            $sql .= " AND (find_in_set('CT002',cat_id) OR find_in_set('CT003',cat_id) or find_in_set('CT004',cat_id) OR find_in_set('CT005',cat_id) OR find_in_set('CT006',cat_id) OR find_in_set('CT007',cat_id) OR find_in_set('CT014',cat_id) OR find_in_set('CT015',cat_id) OR find_in_set('CT016',cat_id) OR find_in_set('CT018',cat_id) OR find_in_set('CT019',cat_id) OR find_in_set('CT021',cat_id) OR find_in_set('CT025',cat_id) OR find_in_set('CT026',cat_id))";
        } elseif ($cat_id == 'CT998') {//女性向けか？
            $sql .= " AND (find_in_set('CT001',cat_id) OR find_in_set('CT047',cat_id) or find_in_set('CT002',cat_id) OR find_in_set('CT031',cat_id))";
        } else {//個別カテゴリ指定か？
            $sql .= " AND find_in_set('$cat_id',cat_id)";
        }
    }
    if ($searchBox != '') {
        $sql .= " AND book_name collate utf8_unicode_ci LIKE '%" . ($searchBox) . "%'";
    }
}
manlog("search_control.php: " . $sql, "sql");
$r_books = mysqli_query($link, $sql);

$chapters = array();
while ($row = mysqli_fetch_array($r_books)) {
    $chapters[] = $row;
}

//カテゴリプルダウンリスト取得
$sql = "SELECT * FROM mz_category FORCE INDEX(idx_del_flg) WHERE del_flg=0";
$result_list_cate = mysqli_query($link, $sql);

$cateNames = array();
while ($arr_list_row = mysqli_fetch_array($result_list_cate)) {
    $cateId = $arr_list_row['cat_id'];
    $cateNames[$cateId] = $arr_list_row['cat_name'];
}
$cnt = 1;
$page = 1;

$image_datas = array();
if (!empty($chapters)) {
    //$image_datas[ページNo(1〜)][表示No(0〜)]の取得
    foreach ($chapters as $chapter) {
        if ($cnt <= _PAGE_MAX) {
            $image_datas[$page][] = $chapter;
            $cnt++;
        } else {
            $page++;
            $cnt = 1;
        }
    }
}
$lastPage = sizeof($image_datas);

//$_REQUEST['PAGE_NUM']はページ番号。
if (!empty($_REQUEST['PAGE_NUM']) && !empty($image_datas[$_REQUEST['PAGE_NUM']])) {

    $imgHtml = "";
    $pageNo = $_REQUEST['PAGE_NUM'];

    if ($_SESSION['read_page_count'] == 0) {//最初のページか？
        $imgHtml .= "<input type='hidden' name='last_page' value='{$lastPage}'>";
    }

    foreach ($image_datas[$_REQUEST['PAGE_NUM']] as $chapter) {
        $pageUrl = "index.php?action=readCnt&b_id=" . $chapter['book_id'] . "&b_name=" . $chapter['book_name'] . "&b_auth=" . $chapter['book_auth'];
        $imgUrl = COMIC_PATH . $chapter['cover_img_path'];
        $bookName = $chapter['book_name'];
        $bookAuth = $chapter['book_auth'];
        $cateIds = explode(",", $chapter['cat_id']);

        $imgHtml .= "<ul class='list clearfix' style='float:left;'>"; //※このulタグが１つの画像
        $imgHtml .= "  <li class='book'>";
        $imgHtml .= "    <p class='thumb'>";
        $imgHtml .= "      <a href='{$pageUrl}'>";
        //【重要】画像の遅延読み込み（lazyload.js）するときはコメント
        //$imgHtml .= "        <img src='{$imgUrl}' alt='{$bookName}' />";
        $imgHtml .= "        <img class='lazy{$pageNo}' data-original='{$imgUrl}' src='images/chapter_loading.gif' alt='{$bookName}' /> ";
        $imgHtml .= "      </a>";
        $imgHtml .= "    </p>";
        $imgHtml .= "    <dl>";
        $imgHtml .= "      <dt class='title'>{$bookName}</dt>";
        $imgHtml .= "      <dd class='name'>{$bookAuth}</dd>";
        $imgHtml .= "    </dl>";

        if (!empty($cateIds)) {
            $imgHtml .= "    <ul class='clearfix'>";
            foreach ($cateIds as $cateId) {
                $cateName = $cateNames[$cateId];
                if (!empty($cateName)) {
                    $cateUrl = "search.php?action=category&cat_id={$cateId}&cat_name={$cateName}{$cateName}";
                    $imgHtml .= "      <li style='width:65px;'>";
                    $imgHtml .= "        <a href='{$cateUrl}'>{$cateName}</a>";
                    $imgHtml .= "      </li>";
                }
            }
            $imgHtml .= "    </ul>";
        }
        $imgHtml .= "  </li>";
        $imgHtml .= "</ul>";
    }

    //【重要】
    //画像の遅延読み込み（lazyload.js）を利用するときは、
    //必ずheadタグ内で <script src="js/read/jquery.lazyload.js"></script> を記載すること。
    /**/
    $imgHtml .= "<script type='text/javascript'>";
    $imgHtml .= "  $(function() {";
    $imgHtml .= "    $('img.lazy{$pageNo}').lazyload({";
    $imgHtml .= "      effect: 'fadeIn',";
    //$imgHtml .= "      effect_speed: 3000";
    $imgHtml .= "      load: function(e){";
    //$imgHtml .= "          callback";
    $imgHtml .= "      }";
    $imgHtml .= "    });";
    $imgHtml .= "  });";
    $imgHtml .= "</script>";
    /**/
    echo $imgHtml;
    $_SESSION['read_page_count'] ++;
} else {
    if ($_SESSION['read_page_count'] == $lastPage) {//ページは全て読み込んだか？
        $_SESSION['read_page_count'] ++;
    }
}
?>

