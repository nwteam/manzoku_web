<?php

ini_set('display_errors', 1);

if (!isset($_GET['title']) || empty($_GET['title'])) {
    echo "title is empty!";
    exit();
}

$nowYmd = date("Y-m-d");
$nowYm = date("Ym");

$targetTitle = $_GET['title'];
$filePath = dirname(__FILE__) . "/log/click" . $nowYm . ".csv";
$targetColumnNo = null;
$countLists = array();

/**
 * CSVファイル読み込み処理
 */
$rfp = @fopen($filePath, "r");
if ($rfp) {
    //CSVファイル有りの時
    while (!feof($rfp)) { // 読み込みデータが無くなるまでループ
        $rdata = trim(fgets($rfp, 4096));

        if (empty($rdata)) {
            break; //データなしで終了
        }

        if (strstr($rdata, '#') !== false) {//見出しか？ 
            $titles = explode(",", $rdata);
            $cnt = 0;
            foreach ($titles as $title) {
                if ($title == $targetTitle) {
                    break;
                }
                $cnt++;
            }
            $targetColumnNo = $cnt;
            $titles[$targetColumnNo] = $targetTitle; //見出しの更新or追加
            continue;
        }

        //カウントデータを日付別の連想配列にセット
        $rdataArray = explode(",", $rdata);
        $date = $rdataArray[0];
        $countLists[$date] = $rdataArray;
    }
    fclose($rfp);

    //カウントデータの回数を更新or初期化
    if (isset($countLists[$nowYmd][$targetColumnNo])) {//同一カラム有り
        $countLists[$nowYmd][$targetColumnNo] ++;
    } else {//同一カラムなし
        if (!isset($countLists[$nowYmd][0])) {//日付データなしか？
            $countLists[$nowYmd] = array_fill(0, sizeof($titles), 0);
            $countLists[$nowYmd][0] = $nowYmd;
        }
        $countLists[$nowYmd][$targetColumnNo] = 1;
    }
} else {

    //CSVファイル無しの時
    $titles = array("#date", $targetTitle);
    $countLists = array(
        $nowYmd => array($nowYmd, 1));
    $targetColumnNo = 1;
}

/**
 * CSVファイル書き込み処理
 */
//見出し
$wdata = "";
$wdata .= implode(",", $titles) . "\n";
//データ
foreach ($countLists as $countList) {
    $wdata .= implode(",", $countList) . "\n";
}
//echo $wdata;

$wfp = @fopen($filePath, "w");
if ($wfp) {
    $ret = @flock($wfp, LOCK_EX); // 排他ロック
    if ($ret == true) {
        fputs($wfp, $wdata);
        flock($wfp, LOCK_UN); // ロック解除
    }
    fclose($wfp);
}
?>