<?php

$nativeAdImgHtml = "";
if (defined('NATIVE_AD') && NATIVE_AD == true) {

    //最大表示広告数
    define("NATIVE_AD_DISP_MAX", 2);

    //ドキュメントルートからの相対パス取得
    $rootPath = str_replace($_SERVER["DOCUMENT_ROOT"], "", dirname(__FILE__));

    //広告データ一覧を取得
    $filename = dirname(__FILE__) . '/ad_list_utf8.csv';
    $handle = fopen($filename, "r");
    $adList = array();

    while ($array = fgetcsv($handle)) {
        if (!empty($array) && is_array($array)) {
            if (substr($array[0], 0, 1) == "#") {
                continue;
            }
            $adList[] = $array;
        }
    }

    //２つの広告で１行とし、最大何行あるか算出する。
    $maxRow = sizeof($adList) / NATIVE_AD_DISP_MAX;

    //表示する広告No（0相対）を算出
    if ($_SESSION['native_ad_no'] === null) {//該当ユーザーはネイティブ広告１行目か？
        //echo "1.該当ユーザーはネイティブ広告１行目。";
        $_SESSION['native_ad_no'] = rand(0, $maxRow - 1); //最初に表示する行（0相対）をランダムで算出
    }
    $adNo = $_SESSION['native_ad_no'] * NATIVE_AD_DISP_MAX; //表示する広告No（0相対）を算出

    if ($adNo > sizeof($adList) - 1) {//広告Noはオーバーか？（最後まで表示完了）
        //echo "3.広告Noはオーバー＆先頭に戻る。";
        $_SESSION['native_ad_no'] = 1; //次に表示する行を2行目にセット
        $adNo = 0; //表示する広告Noを１つめにセット
    } else {
        //echo "2.次の行へ進む。";
        $_SESSION['native_ad_no'] ++; //次の行に進める
    }
    //echo "現adNo=" . $adNo . "/" . "次のnative_ad_no=" . $_SESSION['native_ad_no'] . "<br>";
    //HTMLの生成
    $pageNo = $_REQUEST['PAGE_NUM'];
    $nativeAdImgHtml = "";

    for ($i = 0; $i < NATIVE_AD_DISP_MAX; $i++) {

        $pageUrl = $adList[$adNo][3];
        $logUrl = "/ad_data/five_any/native_x_2/click.php";

        //画像ファイル名から、クリックカウンタの見出し取得
        $logDataTmp = preg_replace("/(.+)(\.[^.]+$)/", "$1", $adList[$adNo][0]);
        $logDataTmps = explode("_", $logDataTmp);
        $logData = $logDataTmps[sizeof($logDataTmps) - 1];

        $imgUrl = $rootPath . "/images/" . $adList[$adNo][0];
        $bookName = $adList[$adNo][1];
        $bookAuth = $adList[$adNo][2];

        $nativeAdImgHtml .= "<ul class='list clearfix' style='float:left;'>"; //※このulタグが１つの画像
        $nativeAdImgHtml .= "  <li class='book'>";
        $nativeAdImgHtml .= "    <p class='thumb'>";
        $nativeAdImgHtml .= "      <a href='javascript:void(0)' onclick=\"log_and_redirect('{$pageUrl}','{$logUrl}', '{$logData}')\" title='{$bookName}'>";
        //$nativeAdImgHtml .= "    <a href='{$pageUrl}' target='_blank' title='{$bookName}'>";
        //【重要】画像の遅延読み込み（lazyload.js）するときはコメント
        //$nativeAdImgHtml .= "        <img src='{$imgUrl}' alt='{$bookName}' />";
        $nativeAdImgHtml .= "        <img class='lazy{$pageNo}' data-original='{$imgUrl}' src='/images/chapter_loading.gif' alt='{$bookName}' /> ";
        $nativeAdImgHtml .= "      </a>";
        $nativeAdImgHtml .= "    </p>";
        $nativeAdImgHtml .= "    <dl>";
        $nativeAdImgHtml .= "      <dt class='title'>{$bookName}</dt>";
        $nativeAdImgHtml .= "      <dd class='name'>{$bookAuth}</dd>";
        $nativeAdImgHtml .= "    </dl>";
        $nativeAdImgHtml .= "    <ul class='clearfix'>";
        $nativeAdImgHtml .= "      <li style='width:65px;'>";
        $nativeAdImgHtml .= "        <a href='javascript:void(0);'>PR</a>";
        $nativeAdImgHtml .= "      </li>";
        $nativeAdImgHtml .= "    </ul>";
        $nativeAdImgHtml .= "  </li>";
        $nativeAdImgHtml .= "</ul>";

        if (++$adNo > sizeof($adList) - 1) {
            break;
        }
    }
}
?>
