<?php

require_once 'aws.phar';

use Aws\Ses\SesClient as SesClient;
use Aws\Common\Enum\Region as Region;
use Aws\Ses\Exception\SesException as SesException;

/** 
 * ----- AmazonSES 利用手順 -----
 * ■AmazonSESの概要
 *【参考】http://dev.classmethod.jp/cloud/amazon-ses-production/
 * 
 * ①Management Consoleにログインする。
 * ②AmazonSES＞Dashboardメニューから「Request Production Access」をクリックし、SES利用申請する。
 * ③SMTP Settingメニューから「Create My SMTP Credentials」をクリックし、SMTPユーザーを作成する。
 *【参考】「IAM User Name」に任意の名称を入力（念のため半角英数）
 *
 * ④登録が完了したら「Show User SMTP Security Credentials」をクリックするとSMTPユーザー名とパスワードが表示。
 *  「Download Credentials」をクリックでSMTPユーザー名とパスワードをダウンロードしておく => CSVファイル
 * 　CSVファイルには「IAM User Name」「Smtp Username」「Smtp Password」が記載されている
 * 
 * ⑤Email Addressesメニューで送信元メールアドレスを認証する
 *【重要】送信元に設定できるのは認証済みのメールアドレスのみ）
 * 
 * ⑥送信元メールアドレスのSPFとTXTレコードに下記例のように記述する（SPFレコードが無い時にはTXTレコードのみ）
 * 　これにより、amazonses.comから送られるメールの送信[ドメイン]は信頼できるという宣言になる。
 *   ＜例＞v=spf1 +ip4:210.172.183.56/29 include:amazonses.com a:deppli2.info -all
 * 
 * ⑦メール内に電子署名を入れて検証（DomainKeys/DKIM）
 * 　Domainメニュー＞「Verify a New Domain」からドメインを登録する。
 * 　画面下部の「Generate DKIM Setting」をクリック
 * 　画面下部にDNSレコード設定情報（CNAMEが３つ）表示されるので、この情報を該当ドメインのレコードに設定する。
 * 
 * ■プログラムの概要
 * ①http://aws.amazon.com の右上の Accountから「Security Credentials」へ移動。
 * 　Access Keys（Access Key ID and Secret Access Key）メニューから「Create New Access Key」をクリック。
 * 　生成された「Access Key ID」「Secret Access Key」を取得する。
 *    ==> プログラム定数「AWS_ACCESS_KEY」と「AWS_SECRET_KEY」にセットする
 * 
 * ②sandbox での動作検証
 * 　a)無料で利用可能。
 * 　b)メールの送信は200通/1日、1通/1秒
 * 　c)認証済みのメールアドレスにしかメールを送信することができない。
 * 
 * ③Production(プロダクション) での本番運用
 * 　a)sandboxで十分な動作確認を行った後、DashbordからProduction(プロダクション)環境を利用する申請を行う。
 * 　b)Production申請の承認が得られた場合、メールの送信は10000通/1日、5通/1秒
 * 　c)認証が得られていないメールアドレスに対してのメール送信が許可される。
 * 
 * ■認証関連
 *【参考】SPF認証（送信元IPアドレスの認証）
 * http://docs.aws.amazon.com/ses/latest/DeveloperGuide/spf.html
 * 
 *【参考】DKIM署名（メール内に電子署名を入れて検証）
 * http://norinoritakanori.blog90.fc2.com/blog-entry-359.html
 * 
 * ■携帯キャリア関連
 * [docomo]
 * https://www.nttdocomo.co.jp/service/communication/imode_mail/notice/sender_id/
 * https://www.nttdocomo.co.jp/service/communication/imode_mail/notice/mass_send/
 *
 * [au]
 * http://www.au.kddi.com/mobile/service/mail/attention
 * http://www.au.kddi.com/mobile/service/mail/attention/request/
 *
 * [softbank]
 * http://www.softbank.jp/mobile/support/antispam/wrestle/
 */

/**
 * AWS-SES利用クラス
 */
class AwsSes {

    const AWS_ACCESS_KEY = 'AKIAJF6TWCUK2NIPGJ6A';
    const AWS_SECRET_KEY = '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';
    const SES_CHARSET = 'ISO-2022-JP';
    const SES_SEND_MAX = 50;

    /**
     * AmazonSES を使ったメール送信
     * 
     * 一度に送信できるメール件数は50件（SESの仕様）
     * 
     * @param string $from     送信元アドレス（AmazonSESで認証されたアドレス）
     * @param array  $toArray  送信先(TO)のアドレス
     * @param string $subject  件名
     * @param string $bodyText 本文
     * @param array  $ccArray  送信先(CC)のアドレス
     * @param array  $bccArray 送信先(BCC)のアドレス
     * @return 結果（array:SES利用の結果、false:失敗）
     */
    public static function sendMailbySes($from, array $toArray, $subject, $bodyText, array $ccArray = array(), array $bccArray = array()) {

        if ((sizeof($toArray) + sizeof($ccArray) + sizeof($bccArray)) > self::SES_SEND_MAX) {
            self::log(__METHOD__ . ": Number of mails that can be sent at one time is " . self::SES_SEND_MAX, 'error');
            return false;
        }

        try {
            /**
             * アクセスキー、シークレットキー、リージョンを指定しクライアントを生成する
             */
            $client = SesClient::factory(
                    array(
                        'key' => self::AWS_ACCESS_KEY,
                        'secret' => self::AWS_SECRET_KEY,
                        'region' => Region::OREGON
                    )
            );

            /**
             * メール送信処理
             */
            $result = $client->sendEmail(array(
                'Source' => $from,
                'ReturnPath' => $from,
                'ReplyToAddresses' => array(
                    'member' => $from),
                'Destination' => array(
                    'ToAddresses' => $toArray,
                    'CcAddresses' => $ccArray,
                    'BccAddresses' => $bccArray,
                ),
                'Message' => array(
                    'Subject' => array(
                        'Data' => $subject,
                        'Charset' => self::SES_CHARSET,
                    ),
                    'Body' => array(
                        'Text' => array(
                            'Data' => $bodyText,
                            'Charset' => self::SES_CHARSET,
                        ),
                    ),
                ),
                )
            );

            self::log(__METHOD__ . ":- " . $result, 'debug');
            return true;
        } catch (SesException $e) { //例外、AmazonSESエラー
            self::log(__METHOD__ . ": " . $e->getMessage(), 'error');
            return false;
        }
    }

    /**
     * ログの出力
     *
     * @param  $message 出力するメッセージ
     * @param  $level   ログレベル
     */
    public static function log($message, $level = 'debug') {
        CakeLog::write($level, $message);
    }

}
