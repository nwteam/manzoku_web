<?php

ini_set('display_errors', 1);
require_once '../util/include.php';

define("AWS_REGIST", 1);
define("AWS_UNREGIST", 2);

/**
 * サブスクリプション（Pushユーザー）の登録
 * 
 * ユーザーテーブルに「subscriptionId」と「endpoint」を登録する
 * AmazonSNSエントリーキューへ登録する
 * 
 * @param type   $link
 * @param string $subscriptionId  レジスターID
 * @param string $endpoint  エンドポイント
 */
function registSubscription($link, $subscriptionId, $endpoint) {

    //登録済みチェック
    $sql = sprintf("SELECT id FROM mz_push_users WHERE subscription_id='%s' LIMIT 1", mysqli_real_escape_string($link, $subscriptionId));
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_array($result);
    if ($row) {//登録済みか？
        return;
    }

    //新規登録処理（※すでにDBがMyISAMで作成されているのでトランザクションは出来ない）
    try {
        //①ユーザーテーブルへの登録
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $sql = sprintf("INSERT INTO mz_push_users (subscription_id, endpoint, user_agent, created) VALUES ('%s', '%s', '%s', '%s')", mysqli_real_escape_string($link, $subscriptionId), mysqli_real_escape_string($link, $endpoint), $userAgent, date('Y-m-d H:i:s'));
        if (!mysqli_query($link, $sql)) {
            deletePushUser($link, $subscriptionId);
        }

        //②AmazonSNSエントリーキューへ登録
        $ret = entrySnsQueue($link, $subscriptionId, '', '', AWS_REGIST);
        if (!$ret) {
            deletePushUser($link, $subscriptionId);
        }
    } catch (Exception $ex) {
        return;
    }
}

/**
 * サブスクリプション（Pushユーザー）の解除
 * 
 * @param type   $link
 * @param string $subscriptionId  レジスターID
 */
function unRegistSubscription($link, $subscriptionId) {
    $sql = sprintf("SELECT endpoint_arn, subscription_arn FROM mz_push_users WHERE subscription_id='%s' LIMIT 1", mysqli_real_escape_string($link, $subscriptionId));
    $result = mysqli_query($link, $sql);
    $row = mysqli_fetch_array($result);
    if ($row) {//取得成功
        deletePushUser($link, $subscriptionId);
        entrySnsQueue($link, $subscriptionId, $row['endpoint_arn'], $row['subscription_arn'], AWS_UNREGIST);
    }
}

/**
 * ユーザーデータの削除
 * 
 * @param type   $link
 * @param string $subscriptionId  レジスターID
 */
function deletePushUser($link, $subscriptionId) {
    $sql = sprintf("DELETE FROM mz_push_users WHERE subscription_id='%s' LIMIT 1", mysqli_real_escape_string($link, $subscriptionId));
    echo $sql;
    mysqli_query($link, $sql);
}

/**
 * AmazonSNSエントリーキューへ登録
 * 
 * @param  type    $link
 * @param  string  $subscriptionId  レジスターID
 * @param  string  $endpointArn
 * @param  string  $subscriptionArn 
 * @param  int     $action          処理内容
 * 
 * @return  結果（true:成功、false:失敗）
 */
function entrySnsQueue($link, $subscriptionId, $endpointArn, $subscriptionArn, $action = AWS_REGIST) {
    try {
        $sql = sprintf("INSERT INTO mz_push_queue (subscription_id, action, endpoint_arn, subscription_arn, created) VALUES ('%s', %d, '%s', '%s', '%s')", mysqli_real_escape_string($link, $subscriptionId), $action, $endpointArn, $subscriptionArn, date('Y-m-d H:i:s'));
        if (!mysqli_query($link, $sql)) {
            return false;
        }
    } catch (Exception $ex) {
        return false;
    }
    return true;
}

/**
 * メイン処理
 */
function main($link) {
    if (!$_POST) {
        echo 'error';
        return;
    }

    //POST値を取得
    $subscriptionId = $_POST['subscription_id'];
    $endpoint = $_POST['endpoint'];
    if (!isset($_POST['action'])) {
        $action = AWS_REGIST;
    } else {
        $action = $_POST['action'];
    }

    if ($action == AWS_REGIST) {
        //Push通知の登録
        registSubscription($link, $subscriptionId, $endpoint);
    } else {
        //Push通知の解除
        unRegistSubscription($link, $subscriptionId);
    }

    echo 'success';
}

main($link);

