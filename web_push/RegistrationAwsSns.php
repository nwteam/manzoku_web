<?php

/**
 * #crontab -u apache -e
 * * * * * * /usr/bin/php /var/www/html/manzoku/web_push/RegistrationAwsSns.php
 */
//ini_set('display_errors', 1);
$zPath = dirname(dirname(__FILE__)) . "/";

require_once $zPath . 'util/include.php';
require_once $zPath . 'web_push/AWS/AwsSns.php';


define("AWS_SNS_REGISTER_MAX", 30); //一度にAmazonSNSに登録するPushIDの最大数

define("AWS_REGIST", 1);
define("AWS_UNREGIST", 2);

/**
 * キューからPushIDを取得
 * 
 * @param type $link
 * @return type
 */
function getSubscriptionIdsInQueue($link) {

    $sql = sprintf("SELECT * FROM mz_push_queue ORDER BY id ASC LIMIT %d", AWS_SNS_REGISTER_MAX);
    $result = mysqli_query($link, $sql);
    $pushIds = array();

    while ($row = mysqli_fetch_array($result)) {
        $pushIds[] = $row;
    }
    return $pushIds;
}

/**
 * キューの中のPushIDを削除
 * 
 * @param type $link
 * @param type $subscriptionIds
 */
function deleteSubscriptionIdsInQueue($link, $subscriptionIds) {

    if (is_array($subscriptionIds)) {
        $subscriptionIdstr = implode(',', $subscriptionIds);
        $subscriptionIdstr = str_replace(",", "','", $subscriptionIdstr);
        $subscriptionIdstr = "'" . $subscriptionIdstr . "'";
        $sql = sprintf("DELETE FROM mz_push_queue WHERE subscription_id IN(%s)", $subscriptionIdstr);
        mysqli_query($link, $sql);
    }
}

/**
 * ログの出力
 *
 * @param  $message 出力するメッセージ
 * @param  $level   ログレベル
 */
function errlog($message, $level = 'debug') {
    error_log($message, 0);
}

/**
 * メイン処理
 * 
 * @param type $link
 */
function main($link) {

    $awsSns = new AwsSns();
    $lists = getSubscriptionIdsInQueue($link);
    $pushIds = array();

    if (sizeof($lists) > 0) {
        foreach ($lists as $list) {

            $appsId = AwsSns::GCM;
            $pushId = $list['subscription_id'];
            $action = $list['action'];
            $pushIds[] = $pushId;

            if ($action == AWS_REGIST) { //AmazonSNSに登録
                $result = $awsSns->createPlatformEndpoint($appsId, $pushId);
                if ($result !== false) { //登録成功か？
                    $sql = sprintf("UPDATE mz_push_users SET endpoint_arn='%s', subscription_arn='%s' WHERE subscription_id='%s' LIMIT 1", $result['EndpointArn'], $result['SubscriptionArn'], $pushId);
                    $ret = mysqli_query($link, $sql);
                    if ($ret !== false) {
                        $ret = $awsSns->setEndpointEnable($result['EndpointArn'], 'true'); //Enable属性をtrue（Push通知可）に変更
                        if (empty($ret)) { //変更失敗か？
                            errlog(__METHOD__ . ": [Cron] EndPointArn の trueセットに失敗しました（AppsID={$appsId}/PushID={$pushId}）", 'error');
                        }
                    } else {
                        errlog(__METHOD__ . ": [Cron] EndPointArn, SubscriptionArnの保存に失敗しました（AppsID={$appsId}/PushID={$pushId}）", 'error');
                    }
                } else { //登録失敗か？
                    errlog(__METHOD__ . ": [Cron] AmazonSNSへの登録に失敗しました（AppsID={$appsId}/PushID={$pushId}）", 'error');
                }
            } else {//AmazonSNSを解除
                $result = $awsSns->deleteEndpoint($list['endpoint_arn'], $list['subscription_arn']);
                if ($result === false) { //解除失敗か？ 
                    errlog(__METHOD__ . ": [Cron] AmazonSNSの解除に失敗しました（AppsID={$appsId}/PushID={$pushId}）", 'error');
                }
            }
        }

        deleteSubscriptionIdsInQueue($link, $pushIds);
    }
}

main($link);
